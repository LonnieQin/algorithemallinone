package com.example.I;

import com.example.algs.StdIn;
import com.example.algs.StdOut;

public class Average {

	/**
	 * @param argsit
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double sum = 0.0;
		int cnt = 0;
		while(!StdIn.isEmpty())
		{
			sum += StdIn.readDouble();
			cnt ++;
		}
		double avg = sum / cnt;
		StdOut.printf("Avg is %.5f",avg);
	}

}

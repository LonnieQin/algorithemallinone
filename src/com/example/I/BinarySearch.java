package com.example.I;

import java.util.Arrays;

public class BinarySearch {

	public static int rank(int key,int[] a)
	{
		int lo = 0;
		int hi = a.length - 1;
		while(lo <= hi)
		{
			int mid = lo + (hi - lo) / 2;
			if		(key < a[mid]) hi = mid - 1;
			else if	(key > a[mid]) lo = mid + 1;
			else					return mid;
		}
		return - 1;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] whitelist = new int[10000];
		for(int i = 0; i < whitelist.length; i++){
			whitelist[i] = (int)(Math.random()*1000);
		}
		Arrays.sort(whitelist);
		for(int i = 0; i < 100; i++){
			System.out.print(whitelist[i]+" ");
		}
		System.out.println();
		int i = (int)(Math.random() * 100);
		System.out.println("position of "+i+":"+rank(i,whitelist));
	}
	
}
package com.example.I;

import com.example.algs.StdOut;

public class Counter {
	private final String name;
	int count;
	public Counter(String id)
	{
		name = id;
		count = 0;
	}
	
	public void increment()
	{	
		count ++;  
	}
	public void decrement()
	{
		count --;
	}
	public int tally()
	{
		return count;
	}
	public String toString()
	{
		return count +" "+ name;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Counter c1 = new Counter("C1");
		c1.increment();
		Counter c2 = c1;
		c2.increment();
		StdOut.println(c1);
		
		Counter heads = new Counter("Heads");
		Counter tails = new Counter("Tails");
		heads.increment();
		heads.increment();
		tails.increment();
		StdOut.println(heads+" "+tails);
		StdOut.println(heads.tally()+tails.tally());
	}

}

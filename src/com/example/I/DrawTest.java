package com.example.I;

import java.awt.Color;
import java.util.Arrays;

import com.example.algs.Array;
import com.example.algs.StdDraw;
import com.example.algs.StdRandom;

public class DrawTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stu
		int [] a = Array.generateRandomNumber(1000, 0, 1000);
		Array.drawArray(a, new Color[]{Color.red,Color.green,Color.blue});
		Arrays.sort(a);
		Array.drawArray(a, new Color[]{Color.black});
	}
	
	private static void test2() {
		// TODO Auto-generated method stub
		int N = 50;
		double [] a = new double[N];
		for(int i = 0; i <N;i++){
			a[i] = StdRandom.random();
			double x = 1.0*i/N;
			double y = a[i]/2;
			double rw = 0.5/N;
			double rh = a[i]/2.0;
			StdDraw.filledRectangle(x, y, rw, rh);
		}
	}

	private static void test3() {
		// TODO Auto-generated method stub
		int N = 50;
		double [] a = new double[N];
		for(int i = 0; i < N; i++)
		{
			a[i] = StdRandom.random();
		}
		Arrays.sort(a);
		for(int i = 0; i < N; i++)
		{
			double x = 1.0*i/N;
			double y = a[i]/2.0;
			double rw = 0.5/N;
			double rh = a[i]/2.0;
			StdDraw.filledRectangle(x, y, rw, rh);
		}
	}

	public static void test1()
	{
		int N = 100;
		StdDraw.setXscale(0, N);
		StdDraw.setYscale(0, N*N);
		StdDraw.setPenRadius(.01);
		for(int i = 0; i < N; i++)
		{
			StdDraw.point(i, i);
			StdDraw.point(i, i*i);
			StdDraw.point(i, i*Math.log(i));
		}
	}

}

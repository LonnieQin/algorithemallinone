package com.example.I;

import com.example.algs.StdOut;
import com.example.algs.StdRandom;

public class Flip {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int T = 1000;
		Counter heads = new Counter("Heads");
		Counter tails = new Counter("Tails");
		for(int t = 0; t < T; t++)
		{
			if(StdRandom.bernoulli(0.5))
			{
				heads.increment();
			}
			else
			{
				tails.increment();
			}
		}
		StdOut.println(heads);
		StdOut.println(tails);
		int d = heads.tally() - tails.tally();
		StdOut.println("Delta:"+Math.abs(d));
		StdOut.println(max(heads,tails)+" win.");
	}
	
	public static Counter max(Counter x,Counter y)
	{
		if(x.tally() > y.tally())	return x;
		else						return y;
	}

}

package com.example.I;

import java.awt.Point;

import com.example.algs.Interval1D;
import com.example.algs.Interval2D;
import com.example.algs.Point2D;
import com.example.algs.StdOut;

public class GeoDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double xlo = .2;
		double xhi = .5;
		double ylo = .5;
		double yhi = .6;
		int T = 10000;
		
		Interval1D x = new Interval1D(xlo,xhi);
		Interval1D y = new Interval1D(ylo,yhi);
		Interval2D box = new Interval2D(x,y);
		box.draw();
		
		Counter c = new Counter("Hits");
		for(int t = 0; t < T; t++)
		{
			double xx = Math.random();
			double yy = Math.random();
			Point2D p = new Point2D(xx,yy);
			if(box.contains(p)) c.increment();
			else 				p.draw();
		}
		StdOut.println(c);
		StdOut.println(box.area());
	}

}

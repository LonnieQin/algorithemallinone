package com.example.I;

import com.example.algs.Out;
import com.example.algs.StdIn;
import com.example.algs.StdOut;
import com.example.algs.collections.Stack;

public class Mathematic {
	public long F[] = new long[1000];
	public Mathematic()
	{
		for(int i = 0; i < F.length; i++)
		{
			F[i] = 0;
		}
	}
	/**
	 * 
	 * @param x:An integer
	 * @return the absolute value of x
	 */
	public static int abs(int x)
	{
		if(x < 0) x = -x;
		return x;
	}
	
	/**
	 * 
	 * @param x:An double value
	 * @return the absolute value of x
	 */
	public static double abs(double x)
	{
		if(x < 0) x = -x;
		return x;
	}
	
	/**
	 * 
	 * @param N:Nature number
	 * @return if the number N is a prime
	 */
	public static boolean isPrime(int N)
	{
		for(int i = 2; i*i < N; i++)
		{
			if(N %i ==0)return false;
		}
		return true;
	}
	
	/**
	 * Square root 
	 * @param c
	 * @return
	 */
	public static double sqrt(double c)
	{
		if(c < 0)return Double.NaN;
		double err = 1e-15;
		double t = c;
		while(Math.abs(t-c/t) > err * t)
			t = (c/t + t) / 2.0;
		return t;
	}
	
	/**
	 * hypotenuse of right retangle
	 * @param a
	 * @param b
	 * @return
	 */
	public static double hypotenuse(double a,double b)
	{
		return Math.sqrt(a*a+b*b);
	}
	
	/**
	 * harmonic number of N
	 * @param N
	 * @return
	 */
	public static double harmonicNumber(int N)
	{
		double sum = 0.0;
		for(int i = 1; i <= N; i++)
			sum += 1.0 / i;
		return sum;
	}
	public static long fibonacci(int n)
	{
		long f=0,g=1;
		if(n == 0) return f;
		else if (n == 1) return g;
		else
		{
			for(int i = 1; i <= n; i++)
			{
				f = f+g;
				g = f-g;
			}
			return f;
		}
	}
	
	/**
	 * Compute the fibonacci number 
	 * @param n
	 * @return
	 */
	public long Fibonacci(int n)
	{
		if(n == 0)
		{
			F[0] = 0;
			return F[0];
		}
		else if(n == 1)
		{
			F[1] = 1;
			return F[1];
		}
		else
		{
			if(F[n] == 0)
			{
				F[n] = fibonacci(n-1)+fibonacci(n-2);
				return F[n];
			}
			return F[n];
		}
	}
	/**
	 * Go
	 */
	static long  GoldFibonacci( int n)
	{
		double a = 1/Math.sqrt(5);
		double golden = (1+Math.sqrt(5))/2;
		double golden2 = (1-Math.sqrt(5))/2;
		double fib = a*(Math.pow(golden, n)-Math.pow(golden2, n));
		return Math.round(fib);
	}
	static long Fa[] = new long[1000];
	public static long factorial(int n)
	{

		if( n == 1 || n == 0) 
		{
			if(Fa[n] == 0)
			{
				Fa[n] = 1;
			}
			return Fa[n];
		}
		else
		{
			if(Fa[n] == 0)
			{
			Fa[n] = factorial(n-1)*n;
			}
			return Fa[n];
		}
	}
	
	public static double LNF[] = new double[1000]; 
	public static double LNFactorial(int N)
	{
		if(N == 1 || N == 0)
		{	
			return 0;
		}
		else
		{
			if(LNF[N] == 0)
			{
				LNF[N] = LNFactorial(N-1)+Math.log(N);
			}
			return LNF[N];
		}
	}
	
	public static int gcd(int p,int q)
	{
		if(q == 0) return p;
		StdOut.println("p:"+p+" q:"+q);
		int r = p % q;
		return gcd(q,r);
	}
	
	public static double binominal(int N,int k,double p)
	{
		System.out.println("binominal("+N+","+k+","+p+");");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(N == 0 && k == 0) return 1.0;
		if(k != 0 && p == 0) return 0.0;
		if(N < 0 || k < 0) return 0.0;
		return (1.0-p)*binominal(N-1,k,p)+p*binominal(N-1,k-1,0);
	}
	
	public static double Evaluate()
	{
		Stack<String> ops = new Stack<String>();
		Stack<Double> vals = new Stack<Double>();
		while(!StdIn.isEmpty())
		{
			String s = StdIn.readString();
			if		(s.equals("(")){}
			else if	(s.equals("+") || 
					 s.equals("-") || 
					 s.equals("*") ||
					 s.equals("sqrt") ||
					 s.equals("**"))
				ops.push(s);
			else if(s.equals(")"))
			{
				String op = ops.pop();
				double v = vals.pop();
				if		(op.equals("+"))	v += vals.pop();
				else if	(op.equals("-"))	v  = vals.pop() - v;
				else if	(op.equals("*"))	v  = vals.pop() * v;
				else if (op.equals("/"))	v  = vals.pop() / v;
				else if (op.equals("sqrt"))	v  = Math.sqrt(v);
				else if (op.equals("**")) 	v = Math.pow(vals.pop(),v);
				vals.push(v);
			}
			else vals.push(Double.parseDouble(s));
		}
		return vals.pop();
	}
	
	public static double Ak(int lo,int hi)
	{
		double sum = 0;
		for(int i = lo; i < hi; i++)
		{
			if(i < 0){
				sum += 1.0/(i-1);
			}
			if(i > 0){
				sum += 1.0/(i+1);
			}
		}
		return sum;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		StdOut.println("ak(-100,100)="+Ak(-100,100));
		System.out.println("Absolute number test:");
		System.out.println("Absolute number of -333 is "+Mathematic.abs(-333));
		System.out.println("Absolute number of 1009 is "+Mathematic.abs(1009));
		System.out.println("Absolute number of -135.89 is "+Mathematic.abs(-135.89));
		System.out.println("Absolute number of 1098.8982 is "+Mathematic.abs(1098.8982));
		System.out.println("Prime number test:");
		for(int i = 2;i < 10000;i++)
		{
			if(isPrime(i))
			{
				System.out.println(i+" is a Prime.");
			}
		}
		System.out.println("Square test:");
		for(int i = 2;i < 100 ;i++)
		{
			System.out.println("Squart root of "+i+" is "+Mathematic.sqrt(i));
		}
		System.out.println("Hypotenuse test:");
		for(int i = 1; i <= 20; i++)
			for(int j = 1; j <= 20; j++)
				System.out.println("The hypotenuse of right trangle("+i+","+j+") is "+Mathematic.hypotenuse(i, j));
		System.out.println("Harmonic Number test:");
		for(int i = 1;i< 100;i++)
		{
			System.out.println("The hamonic number of "+i+" is "+ Mathematic.harmonicNumber(i));
		}
		Out o = new Out("fibtext.txt");
		for(int i = 0; i < 1000; i++)
		{
			o.println(Mathematic.fibonacci(i)+"~"+Mathematic.GoldFibonacci(i));
		}
		o.close();
		*/
		
		String [] opts = new String[]{"(","1","+","(","(","2","+","3",")","*","(","4","*","5",")",")",")"};
		while(true)
		{
			StdOut.println("Pelase input your equation:");
			double result = Mathematic.Evaluate();
			StdOut.println("result:"+result);
		}
		
	}

}

package com.example.I;

import com.example.algs.StdIn;
import com.example.algs.StdOut;
import com.example.algs.collections.Stack;

public class Reverse {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Stack<Integer> stack;
		stack = new Stack<Integer>();
		while(!StdIn.isEmpty())
		{
			stack.push(StdIn.readInt());
		}
		
		for(int i : stack)
		{
			StdOut.println(i);
		}
	}

}

package com.example.I;

import com.example.algs.StdOut;
import com.example.algs.StdRandom;

public class Rolls {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int T = 1000000;
		int SIDES = 6;
		Counter [] rolls = new Counter[SIDES+1];
		for(int i = 0; i < SIDES+1; i++)
		{
			rolls[i] = new Counter(i+"s");
		}
		for(int t = 0; t < T; t++)
		{
			int result = StdRandom.uniform(1, SIDES+1);
			rolls[result].increment();
		}
		for(int i = 1; i < SIDES+1; i++)
		{
			StdOut.println(rolls[i]);
		}
	}

}

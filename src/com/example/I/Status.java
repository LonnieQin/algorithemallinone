package com.example.I;

import com.example.algs.StdIn;
import com.example.algs.StdOut;
import com.example.algs.collections.Bag;

public class Status {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bag<Double> numbers = new Bag<Double>();
		for(int i = 0; i < 100; i++)
		{
			numbers.add((double)i);
		}
		int N = numbers.size();
		double sum = 0.0;
		for(double x:numbers)
			sum += x;
		double mean = sum/N;
		
		sum = 0.0;
		for(double x:numbers)
			sum += (x-mean)*(x-mean);
		double std = Math.sqrt(sum/N-1);
		StdOut.printf("Mean %.2f", mean);
		StdOut.printf("Std dev:%.2f\n", std);
	}

}

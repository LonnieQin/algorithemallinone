package com.example.I;

import java.util.Arrays;

import com.example.algs.Array;
import com.example.algs.In;
import com.example.algs.Out;
import com.example.algs.StdOut;
import com.example.algs.StdRandom;
import com.example.algs.StopWatch;

public class ThreeSum {

	public static int count(int [] a)
	{
		Arrays.sort(a);
		int N = a.length;
		int cnt = 0;
		for(int i = 0; i < N; i++)
			for(int j = i+1; j < N; j++)
			{
				if(Array.binarySearch(-a[i]-a[j], a) > i)
				{
						cnt++;
				}
			}
		return cnt;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Out o = new Out("1Mints.txt");
		int N = 8000;
		StopWatch watch = new StopWatch();
		for(int i = 0; i < N;i++)
		{
			o.print(StdRandom.uniform(-1000000, 1000000)+" ");
			if(i%100==0)o.println();
		}
		StdOut.println("Generate time:"+watch.elapsedTime());
		o.close();
		int [] a = In.readInts("1Mints.txt");
		watch.reset();
		StdOut.println(count(a));
		StdOut.println("Count time:"+watch.elapsedTime());
	}

}

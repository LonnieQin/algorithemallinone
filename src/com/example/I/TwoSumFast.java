package com.example.I;

import java.util.Arrays;

import com.example.algs.Array;
import com.example.algs.StdOut;
import com.example.algs.StopWatch;

public class TwoSumFast {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int max = 1000000;
		StopWatch watch = new StopWatch();
		for(int N = 250; N <= 1000000; N*=2){
			int a [] = Array.generateRandomNumber(N, -max, max);
			watch.reset();
			Arrays.sort(a);
			StdOut.println("Sort Time:"+watch.elapsedTime());
			int cnt = 0;
			watch.reset();
			for(int i = 0; i < N;i++)
			{
				if(Array.binarySearch(-a[i], a) > i)
					cnt ++;
			}
			StdOut.println("Time:"+watch.elapsedTime()+" count:"+cnt);
		}
	}

}

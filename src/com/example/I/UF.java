package com.example.I;

import com.example.algs.Out;
import com.example.algs.StdIn;
import com.example.algs.StdOut;
import com.example.algs.StdRandom;
import com.example.algs.StopWatch;

public class UF {

	private int count;
	private int [] id;
	/**
	 * Intitialize N sites with interger names
	 * @param N number of sides
	 */
	public UF(int N) 
	{
		// TODO Auto-generated constructor stub
		id = new int[N];
		for(int i = 0; i < N; i++)
		{
			id[i] = i;
		}
		count = N;
	}
	
	/**
	 * Add connection between p and q
	 * @param p site
	 * @param q site
	 */
	public void union(int p,int q)
	{
		int pRoot = find(p);
		int qRoot = find(q);
		if(pRoot == qRoot) return;
		id[pRoot] = qRoot;
		count --;
	}
	
	public int find(int p)
	{
		while(p != id[p]) p = id[p];
		return p;
	}
	
	

	/**
	 * return true if p and q are in the same component
	 * @param p site
	 * @param q site
	 * @return
	 */
	public boolean connected(int p, int q)
	{
		return find(p) == find(q);
	}
	
	/**
	 * Number of components
	 * @return
	 */
	public int count()
	{
		return count;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double prev = randomUF(125,true);
		for(int N = 250; N < 100000000; N*=2)
		{
			double present = randomUF(N,false);
			double rate = present/prev;
			StdOut.println(N+"number "+present+"s "+" rate:"+rate);
			prev = present;
		}
	}
	static double randomUF(int N,boolean file)
	{
		UF uf = new UF(N);
		int [] p = new int[N];
		int [] q = new int[N];
		Out o = null,o1 = null;
		if(file)
			o = new Out("ufpair+"+N+".txt");
		int count = 0;
		StopWatch sw = new StopWatch();
		for(int i = 0; i < N; i++)
		{
			int random = StdRandom.uniform(0, N);
			if(random != i)
			{
				p[i] = i;
				q[i] = random;
				if(file)
					o.print("("+p[i]+","+q[i]+") ");
				count ++;
				if((count % 100) == 0 && file)
						o.println();
			}
		}
		if(file)o.close();
		if(file)o1 = new Out("uffind"+N+".txt");
		for(int i = 0; i < count; i++)
		{
			if(uf.connected(p[i], q[i])) continue;
			uf.union(p[i], q[i]);
			if(file)o1.println(p[i] +" " + q[i]+" "+uf.count());
		}
		if(file)o1.close();
		StdOut.println("Count:"+uf.count());
		return sw.elapsedTime();
	}

}

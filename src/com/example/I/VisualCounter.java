package com.example.I;

import com.example.algs.StdDraw;

public class VisualCounter {
	private final Counter counter;
	private int time;
	private int max;
	private int N;
	public VisualCounter(String id,int N,int max) {
		// TODO Auto-generated constructor stub
		counter = new Counter(id);
		this.time = 0;
		this.N = N;
		this.max = max;
		StdDraw.setScale(0, max);
		StdDraw.setPenColor(StdDraw.BOOK_BLUE);
	}
	public void increment()
	{
		if(time > N){
			draw();
			return;
		}
		if(counter.count < max)
		{
			counter.increment();
		}
		draw();
		time++;
	}
	
	public void decrement()
	{
		if(time > N){
			draw();
			return;
		}
		if(counter.count > 0)
		{	
			counter.decrement();
		}
		draw();
		time++;
	}
	public void draw()
	{
		StdDraw.rectangle(time*(time+0.5)/N,count()/2 , 0.5/N, count()/2);
	}
	
	public int count()
	{
		return counter.count;
	}
}

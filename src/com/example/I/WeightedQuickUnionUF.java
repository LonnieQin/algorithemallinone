package com.example.I;

import com.example.algs.Out;
import com.example.algs.StdOut;
import com.example.algs.StdRandom;
import com.example.algs.StopWatch;

public class WeightedQuickUnionUF {

	private int [] id;
	private int [] sz;
	private int count;
	public WeightedQuickUnionUF(int N) {
		// TODO Auto-generated constructor stub
		count = N;
		id = new int[N];
		for	(int i = 0; i < N; i++)id[i] = i;
		sz = new int[N];
		for	(int i = 0; i < N; i++)sz[i] = 1;
	}
	
	public int count()
	{	return count;	}

	public boolean connected(int p, int q)
	{	return find(p) == find(q);	}
	
	private int find(int p)
	{
		while(p != id[p]) p = id[p];
		return p;
	}
	
	public void union(int p, int q)
	{
		int i = find(p);
		int j = find(q);
		if(i == j) return;
		
		if(sz[i] < sz[j])	{id[i] = j; sz[j] += sz[i]; }
		else				{id[j] = i;	sz[i] += sz[j];	}
		count --;
	}
	static double randomUF(int N,boolean file)
	{
		WeightedQuickUnionUF uf = new WeightedQuickUnionUF(N);
		int [] p = new int[N];
		int [] q = new int[N];
		Out o = null,o1 = null;
		if(file)
			o = new Out("ufpair+"+N+".txt");
		int count = 0;
		StopWatch sw = new StopWatch();
		for(int i = 0; i < N; i++)
		{
			int random = StdRandom.uniform(0, N);
			if(random != i)
			{
				p[i] = i;
				q[i] = random;
				if(file)
					o.print("("+p[i]+","+q[i]+") ");
				count ++;
				if((count % 100) == 0 && file)
						o.println();
			}
		}
		if(file)o.close();
		if(file)o1 = new Out("uffind"+N+".txt");
		for(int i = 0; i < count; i++)
		{
			if(uf.connected(p[i], q[i])) continue;
			uf.union(p[i], q[i]);
			if(file)o1.println(p[i] +" " + q[i]+" "+uf.count());
		}
		if(file)o1.close();
		StdOut.println("Count:"+uf.count());
		return sw.elapsedTime();
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double prev = randomUF(125,true);
		for(int N = 250; N < 5000000; N*=2)
		{
			double present = randomUF(N,false);
			double rate = present/prev;
			StdOut.println(N+"number "+present+"s "+" rate:"+rate);
			prev = present;
		}
	}

}

package com.example.II;

import com.example.algs.Date;
import com.example.algs.Out;
import com.example.algs.StdRandom;
import com.example.algs.sort.Selection;

public class DateSortSample {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Date [] d = new Date[1000];
		for(int i = 0; i < d.length; i++)
		{
			int year = StdRandom.uniform(2000,2100);
			int month = StdRandom.uniform(1, 13);
			int day;
			switch(month)
			{
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				day = StdRandom.uniform(1, 32);
				break;
			case 4:
			case 6:
			case 9:
			case 11:
				day = StdRandom.uniform(1,31);
				break;
			case 2:
				if(year % 4 == 0  )
				{
					day = StdRandom.uniform(1,30);
					if(year % 100 == 0 && year % 400 != 0)
						day = StdRandom.uniform(1,29);
				}
				else
					day = StdRandom.uniform(1,29);
				break;
			default:
				day = 1;
				break;
			}
			d[i] = new Date(month,day,year);
		}
		new Selection().sort(d);
		Out o = new Out("dates.txt");
		for(int i = 0; i < d.length; i++)
		{
			o.println(d[i].toString());
		}
		o.close();
	}

}

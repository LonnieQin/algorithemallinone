package com.example.II;

import com.example.algs.In;
import com.example.algs.Out;
import com.example.algs.StdOut;
import com.example.algs.StdRandom;
import com.example.algs.StdString;
import com.example.algs.StopWatch;
import com.example.algs.sort.Selection;
import com.example.algs.sort.Sorter;

public class Example {

	public static void sort(Comparable [] a)
	{	new Selection().sort(a);									}
	private static boolean less(Comparable v, Comparable w)
	{
		return v.compareTo(w) < 0;
	}
	
	private static void exch(Comparable [] a, int i, int j)
	{	Comparable t = a[i]; a[i] = a[j]; a[j] = t;}
	private static void show(Comparable [] a)
	{
		for(int i = 0; i < a.length; i++)
			StdOut.print(a[i]+" ");
		StdOut.println();
	}
	
	public static boolean isSorted(Comparable [] a)
	{
		for(int i = 1; i < a.length; i++)
			if(less(a[i],a[i-1])) return false;
		return true;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		String [] a = StdString.generateRandomString(1000, 3, 10);
		Selection.sort(a, true);
		StdOut.println("Is sorted:"+isSorted(a));
		StdOut.println(a.length);
		Out o = new Out("sortedString.txt");
		for(int i = 0; i < a.length; i++)
		{
			o.println(a[i]);
		}
		o.println();
		o.close();
		*/
		int N = 125;
		StopWatch sw = new StopWatch();
		doublingTest(N);
		double prev = sw.elapsedTime();
		sw.reset();
		for(N = 250;N<100000;N*=2)
		{
			doublingTest(N);
			double present = sw.elapsedTime();
			double rate = present/prev;
			StdOut.println(N+" "+present+" "+ rate);
			prev = present;
			
		}
	}
	protected static void doublingTest(int N)
	{
		Double [] doubleNums = new Double[N];
		for(int i = 0; i < N; i++)
			doubleNums[i] = StdRandom.uniform(0.0, 100.0);
		new Selection().sort(doubleNums);
		
	}

}

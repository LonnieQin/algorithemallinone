package com.example.III;

import java.util.Comparator;

public class Word implements Comparable<Word>{
	public String word;
	public int frequence;
	public static Comparator<Word> SortByWord = new Comparator<Word>(){

		@Override
		public int compare(Word arg0, Word arg1) {
			// TODO Auto-generated method stub
			return arg0.word.compareTo(arg1.word);
		}

	};
		
	public static Comparator<Word> FrenquenceAsend = new Comparator<Word>(){

		@Override
		public int compare(Word arg0, Word arg1) {
			// TODO Auto-generated method stub
			if(arg0.frequence > arg1.frequence)			return +1;
			else if(arg0.frequence < arg1.frequence)	return -1;
			else 										return  0;
		}

	};
	public static Comparator<Word> FrenquenceDecrease = new Comparator<Word>(){

		@Override
		public int compare(Word arg0, Word arg1) {
			// TODO Auto-generated method stub
			if(arg0.frequence > arg1.frequence)			return -1;
			else if(arg0.frequence < arg1.frequence)	return +1;
			else 										return  0;
		}

	};

		@Override
		public int compareTo(Word that) {
			// TODO Auto-generated method stub
			if(this.frequence > that.frequence)			return +1;
			else if(this.frequence < that.frequence)	return -1;
			else										return  0;
		}		
		
}

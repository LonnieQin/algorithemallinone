package com.example.IV;

import com.example.algs.StdOut;
import com.example.algs.collections.Bag;
import com.example.algs.graph.Graph;

public class Search {
	public int vertexs[];
	public Search(Graph G,int s) {
		// TODO Auto-generated constructor stub
		int count = 0;
		Bag<Integer> b = (Bag<Integer>) G.adj(s);
		for(int i:b){
			count++;
		}
		vertexs = new int[count];
		count = 0;
		for(int i:b){
			vertexs[count++] = i;
		}
	}
	
	public boolean marked(int v)
	{
		boolean marked = false;
		for(int i = 0; i < vertexs.length; i++){
			if(vertexs[i] == v)
				marked = true;
		}
		return marked;
	}
	
	public int count()
	{
		return vertexs.length;
	}

}

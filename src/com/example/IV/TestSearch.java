package com.example.IV;

import com.example.algs.In;
import com.example.algs.StdOut;
import com.example.algs.graph.Graph;

public class TestSearch {

	public static void main(String [] args){
		//Graph g = Graph.fromString(new In("graph116V3350E.txt"));
		Graph g = new Graph(new In("tinyCG.txt"));
		int s = 1;
		Search search = new Search(g,s);
		for(int v = 0; v < g.V(); v++){
			if(search.marked(v))
				StdOut.print(v+" ");
		}
		StdOut.println();
		if(search.count() != g.V())
			StdOut.print("Not ");
		StdOut.println("Connected");
	}

}

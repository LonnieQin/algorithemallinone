package com.example.V;

import com.example.algs.In;
import com.example.algs.StdOut;
import com.example.algs.StopWatch;
import com.example.algs.string.Alphabet;

public class Count {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Alphabet alpha = new Alphabet("ABC");
		int R = alpha.R();
		int [] count = new int[R];
		String s = "ABCCC";
		int N = s.length();
		for	(int i = 0; i < N; i++)
			if(alpha.contains(s.charAt(i)))
				count[alpha.toIndex(s.charAt(i))]++;
		
		for(int c = 0; c < R; c++)
			StdOut.println(alpha.toChar(c)+" "+count[c]);
		In in = new In("world192.txt");
		StopWatch timer = new StopWatch();
		StringBuilder sb = new StringBuilder();
		for(String str:in.readAllStrings())
			sb.append(str);
		alpha = new Alphabet(sb.toString());
		StdOut.println(alpha);
		StdOut.println("Total Time:"+timer.elapsedTime());
		
	}

}

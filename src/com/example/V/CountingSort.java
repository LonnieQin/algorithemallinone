package com.example.V;

import com.example.algs.Array;

public class CountingSort {

	public CountingSort(int a[],int R) {
		// TODO Auto-generated constructor stub
		int N = a.length;
		int [] count = new int[R+1];
		int [] aux = new int[N];
		for	(int i = 0; i < N; i++)
			count[a[i]+1]++;
		for	(int r = 0; r < R; r++)
			count[r+1] += count[r];
		for(int i = 0; i < N; i++)
			aux[count[a[i]]++] = a[i];
		for(int i = 0; i < N; i++)
			a[i] = aux[i];
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int R = 10;
		int [] a = Array.generateRandomNumber(100, 0, R);
		new CountingSort(a,R);
		Array.print(a);
	}

}

package com.example.algs;

public class Accumulator {

	private double total;
	private int N;
	public Accumulator()
	{
		total = 0;
		N = 0;
	}
	public void addDataValue(double val)
	{
		N++;
		total += val;
	}
	public int getN()
	{
		return N;
	}
	public double getTotal()
	{
		return total;
	}
	
	public double mean()
	{	return total/N;}
	
	public String toString()
	{	return "Mean ("+N+" values):"+String.format("%7.5f", mean());}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int T = 10000;
		Accumulator a = new Accumulator();
		for (int i = 0; i < T; i++)
			a.addDataValue(StdRandom.random());
		StdOut.println(a);
	}

}

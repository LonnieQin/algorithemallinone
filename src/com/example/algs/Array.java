package com.example.algs;

import java.awt.Color;


public class Array {
	/**
	 * create an array with random number at the range betewwn min to max
	 * @param length array length
	 * @param min min number
	 * @param max max number
	 * @return array
	 */
	public static int [] generateRandomNumber(int length,int min,int max){
		int a [] = new int[length];
		for(int i = 0; i < a.length; i++)
		{
			a[i] = (int)(Math.random()*(max-min)) + min;
		}
		return a;
	}
	
	public static Integer [] generateRandomInteger(int length,int min,int max){
		Integer a [] = new Integer[length];
		for(int i = 0; i < a.length; i++)
		{
			a[i] = (int)(Math.random()*(max-min)) + min;
		}
		return a;
	}
	
	/**
	 * switch int [] to Integer []
	 * @param ints
	 * @return
	 */
	public static Integer [] IntsToIntegers(int [] ints){
		Integer integers [] = new Integer[ints.length];
		for(int i = 0; i < ints.length; i++)
		{
			integers[i] = ints[i];
		}
		return integers;
	}
	
	/**
	 * switch Integer [] to int []
	 * @param integers
	 * @return
	 */
	public static int [] IntegersToInts(Integer [] integers){
		int ints [] = new int[integers.length];
		for(int i = 0; i < ints.length; i++)
		{
			ints[i] = integers[i];
		}
		return ints;
	}
	
	/**
	 * find max value of array
	 * @param a:array name
	 * @return max value of array
	 */
	public static int max(int [] a)
	{
		if(a != null && a.length > 0)
		{
			int max = a[0];
			for(int i = 0;i<a.length;i++ )
			{
				if(a[i] > max) max = a[i];
			}
			return max;
		}
		return -1;
	}
	
	/**
	 * average value of array
	 * @param a:array name
	 * @return average value
	 */
	public static double avg(int [] a)
	{
		if(a != null && a.length > 0)
		{
			int sum = 0;
			for(int i = 0; i < a.length; i++)
			{
				sum += a[i];
			}
			return (double)(sum)/a.length;
		}
		return -1;
	}
	
	/**
	 * copy an array to another array
	 * @param a:the array to be copyed
	 * @return:new array with the same value as a
	 */
	public static int [] copy(int [] a)
	{
		int [] newArray = new int[a.length];
		for(int i = 0; i < newArray.length; i++)
		{
			newArray[i] = a[i];
		}
		return newArray;
	}
	
	/**
	 * Reverse an array
	 * @param a:Array name 
	 * @return the reversed array
	 */
	public static int[] reverse(int [] a)
	{
		for(int i = 0; i < a.length/2; i++)
		{
			int temp = a[i];
			a[i] = a[a.length-1-i];
			a[a.length-1-i] = temp;
		}
		return a;
	}
	
	/**
	 * print an 1d array
	 * @param a
	 */
	public static void print(int [] a)
	{
		if(a !=null && a.length > 0)
		{
			for(int i = 0; i < a.length;i++) System.out.print(a[i]+" ");
			System.out.println();
		}
	}
	/**
	 * print an 2d array
	 * @param a
	 */
	public static void print(int [][] a)
	{
		if(a !=null && a.length > 0)
		{
			for(int i = 0; i < a.length;i++) 
			{
				for(int j = 0;j< a[i].length;j++)
				{
					System.out.print(a[i][j]+" ");
				}
				System.out.println();
			}
			System.out.println();
		}
	}
	
	public static int [][] martrixMultiplication(int a[][],int b[][])
	{
		if(a[0].length == b.length)
		{
			int [][] matrix = new int[a.length][b[0].length];
			for(int i = 0; i < a.length;i++)
			{
				for(int j = 0; j < b[0].length;j++)
				{
					matrix[i][j] = 0;
				}
			}
			for(int i = 0; i < a.length; i++)
			{
				for(int j = 0; j < b[i].length; j++)
				{
					for(int k = 0; k < b.length; k++)
					{
						matrix[i][j] += a[i][k]*b[k][j];
					}
				}
			}
			return matrix;
		}
		return null;
	}
	public static void drawArray(int a[]) {
		// TODO Auto-generated method stub
		int max = 0;
		for(int i = 0; i < a.length; i++)
		{
			if(a[i] > max) max = a[i];
		}

		for(int i = 0; i <a.length;i++){
			double x = 1.0*i/a.length;
			double y = a[i]/(2.0*max);
			double rw = 0.5/a.length;
			double rh = a[i]/(2.0*max);
			StdDraw.filledRectangle(x, y, rw, rh);
		}
	}	
	public static void drawArray(int a[],Color color) {
		// TODO Auto-generated method stub
		StdDraw.setPenColor(color);
		int max = 0;
		for(int i = 0; i < a.length; i++)
		{
			if(a[i] > max) max = a[i];
		}

		for(int i = 0; i <a.length;i++){
			double x = 1.0*i/a.length;
			double y = a[i]/(2.0*max);
			double rw = 0.5/a.length;
			double rh = a[i]/(2.0*max);
			StdDraw.filledRectangle(x, y, rw, rh);
		}
	}	

	
	public static void drawArray(int a[],Color [] colors) {
		// TODO Auto-generated method stub
		int max = 0;
		for(int i = 0; i < a.length; i++)
		{
			if(a[i] > max) max = a[i];
		}

		for(int i = 0; i <a.length;i++){
			double x = 1.0*i/a.length;
			double y = a[i]/(2.0*max);
			double rw = 0.5/a.length;
			double rh = a[i]/(2.0*max);
			StdDraw.setPenColor(colors[i%colors.length]);
			StdDraw.filledRectangle(x, y, rw, rh);
		}
	}

	public static boolean [][] generateBoolean2DArray(int width,int height)
	{
		boolean [][] booleanArray = new boolean[height][width];
		for(int i = 0; i < height; i++)
		{
			for(int j = 0; j < width; j++)
			{
				booleanArray[i][j] = (StdRandom.uniform(1,100)%2==0)?true:false;
				StdOut.println("a["+i+"]["+j+"]="+booleanArray[i][j]);
			}
		} 
		return booleanArray;		
	}	
	public static void printBoolean2DArray(boolean [][] booleanArray,char t,char f)
	{
		for(int i = 0; i < booleanArray.length; i++)
		{
			for(int j = 0; j < booleanArray[i].length; j++)
			{
				char c = (booleanArray[i][j] == true)?t:f;
				StdOut.print(c);
			}
			StdOut.println();
		}
		StdOut.println();
	}
	public static int binarySearch(int key,int a[])
	{
		return binarySearch(key,a,0,a.length-1,0);
	}
	public static int binarySearch(int key,int a[],int lo,int hi,int depth)
	{
		if(lo > hi) return -1;
		int mid = lo +(hi-lo)/2;
		if(key > a[mid])
		{
			//System.out.println("low:"+lo+" high:"+hi+" depth:"+depth);
			return binarySearch(key,a,mid+1,hi,depth+1);
		}
		else if( key < a[mid])
		{
			//System.out.println("low:"+lo+" high:"+hi+" depth:"+depth);
			return binarySearch(key,a,lo,mid-1,depth+1);
		}
		else
		{
			return mid;
		}
	}
}

package com.example.algs;

public interface Datable {
	public int month();
	public int day();
	public int year();
}

package com.example.algs;

public class Date implements Datable,Comparable<Date>{

	private final int value;
	public Date()
	{
			int y = StdRandom.uniform(2000,2100);
			int m = StdRandom.uniform(1, 13);
			int d;
			switch(m)
			{
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				d = StdRandom.uniform(1, 32);
				break;
			case 4:
			case 6:
			case 9:
			case 11:
				d = StdRandom.uniform(1,31);
				break;
			case 2:
				if(y % 4 == 0  )
				{
					d = StdRandom.uniform(1,30);
					if(y % 100 == 0 && y % 400 != 0)
						d = StdRandom.uniform(1,29);
				}
				else
					d = StdRandom.uniform(1,29);
				break;
			default:
				d = 1;
				break;
		}
		value = y*512 +m*32+d;	
	}
	
	public Date(int m, int d, int y)
	{
		value = y*512 +m*32+d;
	}
	public Date(String s)
	{
		String [] strs = s.split("/");
		int m = Integer.parseInt(strs[0]);
		int d = Integer.parseInt(strs[1]);
		int y = Integer.parseInt(strs[2]);
		value = y*512+m*32+d;
	}
	public int month()
	{return (value/32)%16;}
	public int day()
	{return value % 32;}
	public int year()
	{return value / 512;}
	public String toString()
	{
		return month() +"/" + day() +"/" + year();
	}
	
	public boolean equals(Object x)
	{
		if(this == x) 								return true;
		if(x == null) 								return false;
		if(this.getClass() != x.getClass())			return false;
		Date that = (Date)x;
		if(!this.toString().equals(that.toString()))return false;
		return true;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Date d = new Date(7,30,2016);
		Date e = new Date(7,30,2016);
		System.out.println(d==e);
		System.out.println(d.equals(e));
		Object a = new Object();
		Object b = new Object();
		Object c = new Object();
		a = b;
		System.out.println(a+" "+b);
		a = c;
		System.out.println(a+" "+b);
	}
	@Override
	public int compareTo(Date that) {
		// TODO Auto-generated method stub
		if(this.year() > that.year()) return +1;
		if(this.year() < that.year()) return -1;
		if(this.month() > that.month()) return +1;
		if(this.month() < that.month()) return -1;
		if(this.day() > that.day()) return +1;
		if(this.day() < that.day()) return -1;
		return 0;
	}

}

package com.example.algs;

import com.example.algs.StdOut;
import com.example.algs.StdRandom;

public class MinPQ<Key extends Comparable<Key>> {

	public MinPQ() {
		// TODO Auto-generated constructor stub
		pq = (Key[])new Comparable[2];
	}
	private Key[] pq;	//heap-ordered complete binary tree
	private int N = 0;	//pq[1..N] with pq[0] unused
	public boolean isEmpty()
	{
		return N==0;
	}
	public int size()
	{	return N;	}
	public void insert(Key v)
	{
		pq[++N] = v;
		if(N == pq.length-1)
			resize(2*N+1);
		swim(N);
	}
	
	private void resize(int n) {
		// TODO Auto-generated method stub
		Key [] newPq = (Key[]) new Comparable[n];
		for(int i = 1; i < pq.length;i++){
			newPq[i] = pq[i];
		}
		pq = newPq;
	}
	public Key delMin()
	{
		Key min = pq[1];	//Retrieve max key from top
		exch(1,N--);		//Exchange with last item.
		pq[N+1] = null;		//Avoid loitering.
		sink(1);			//Restore heap property
		return min;
	}
	private void sink(int k) {
		// TODO Auto-generated method stub
		while(2*k <= N)
		{
			int j = 2*k;
			if(j < N && larger(j,j+1)) j++;
			if(!larger(k,j)) break;
			exch(k,j);
			k = j;
		}
	}
	private boolean larger(int k, int j) {
		// TODO Auto-generated method stub
		return pq[k].compareTo(pq[j]) > 0;
	}
	private void exch(int i, int j) {
		// TODO Auto-generated method stub
		Key t = pq[i];
		pq[i] = pq[j];
		pq[j] = t;
	}
	private void swim(int k) {
		// TODO Auto-generated method stub
		while (k > 1 && larger(k/2,k))
		{
			exch(k/2,k);
			k = k / 2;
		}
	}
	
	public static void main(String [] args)
	{
		int N = 100;
		MinPQ<Integer> pq = new MinPQ<Integer>();
		for(int i = 0; i < N; i++)
		{
			int n = StdRandom.uniform(500);
			pq.insert(n);
			StdOut.print(n+" ");
		}
		StdOut.println();
		while(pq.size() != 0)
		{
			StdOut.print(pq.delMin()+" ");
		}
		StdOut.println();
	}	
}

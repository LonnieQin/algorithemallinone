package com.example.algs;

import java.util.Arrays;


public class StaticSetOfInts {
	private int [] a;
	public StaticSetOfInts(int [] keys)
	{
		a = new int[keys.length];
		for(int i =0; i < keys.length; i++)
			a[i] = keys[i];
		Arrays.sort(a);
	}
	public boolean contains(int key)
	{
		return (rank(key) != -1);
	}
	private int rank(int key) {
		// TODO Auto-generated method stub
		int lo = 0;
		int hi = a.length - 1;
		while (lo <= hi)
		{
			int mid = lo+(hi-lo)/2;
			if		(key < a[mid]) hi = mid - 1;
			else if	(key > a[mid]) lo = mid + 1;
			else					return mid;
		}
		return -1;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] w = Array.generateRandomNumber(10000, 0, 111);
		Out out = new Out("whitelist.txt");
		for(int i = 0; i < w.length; i++)
		{
			out.print(i+" ");
		}
		StaticSetOfInts set = new StaticSetOfInts(w);
		while(!StdIn.isEmpty())
		{
			int key = StdIn.readInt();
			if(!set.contains(key))
			{
				StdOut.println(key);
			}
		}
	}

}

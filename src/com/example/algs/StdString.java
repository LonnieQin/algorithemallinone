package com.example.algs;

public class StdString {

	public static boolean isPalindrome(String s)
	{
		int N = s.length();
		for(int i = 0; i < N/2; i++)
		{
			if(s.charAt(i) != s.charAt(N-1-i))
			{
				return false;
			}
		}
		return true;
	}
	
	public static boolean isSorted(String [] a)
	{
		for(int i = 1; i < a.length; i++)
		{
			if(a[i-1].compareTo(a[i]) > 0)
			{
				return false;
			}
		}
		return true;
	}
	
	public static String [] splictToLines()
	{
		String input = StdIn.readAll();
		String [] words = input.split("\\s+");
		return words;
	}
	
	public static boolean isCircularShift(String a,String b)
	{
		if(a.length() != b.length())	return false;
		for(int i = 1; i < a.length()-1; i++)
		{
			String left = a.substring(0, i);
			String right = a.substring(i, a.length());
			String shiftedStr = right.concat(left);
			System.out.println(shiftedStr);
			if(shiftedStr.equals(b))	return true;
		}
		return false;
	}
	
	public static String reverse(String str)
	{
		int N = str.length();
		if(N <= 1)	return str;
		String a = str.substring(0,N/2);
		String b = str.substring(N/2,N);
		return reverse(b)+reverse(a);
	}
	
	public static String [] generateRandomString(int N,int minlen,int maxlen)
	{
		String [] a = new String[N];
		for(int i = 0; i < N; i++)
		{
			int length = StdRandom.uniform(minlen,maxlen+1);
			StringBuilder sb = new StringBuilder();
			for(int j = 0; j < length; j++)
			{
				char c = (char)StdRandom.uniform('a', 'z'+1);
				sb.append(c);
			}
			a[i] = sb.toString();
		}
		return a;
	}
	public static void saveToFile(String [] strs,String fileName)
	{
		Out out = new Out(fileName);
		In in = new In(fileName);
		for(String str:strs)
			out.println(str);
		out.close();
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String [] strs = new String[]{"AAA","BBB","Ccc","DDD","EEEE","fff"};
		/*
		System.out.println(isSorted(strs));
		String [] strs2 = splictToLines();
		for(int i = 0; i < strs2.length; i++)
		{
			StdOut.println(strs2[i]);
		}
		*/
		
		StdOut.println("is shifted:"+StdString.isCircularShift("abcde", "cdeab"));
	}

}

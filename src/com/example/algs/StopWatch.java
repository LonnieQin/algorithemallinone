package com.example.algs;

public class StopWatch {
	public long start;
	public StopWatch() {
		// TODO Auto-generated constructor stub
		reset();
	}
	public void reset(){
		start = System.currentTimeMillis();
	} 
	public double elapsedMillis(){
		return (System.currentTimeMillis() - start);
	}
	public double elapsedTime(){
		return (System.currentTimeMillis() - start)/1000;
	}

}

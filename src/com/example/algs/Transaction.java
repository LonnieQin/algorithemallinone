package com.example.algs;

import java.util.Arrays;
import java.util.Comparator;

public class Transaction implements Comparable<Transaction>{
	String who;
	Date when;
	double amount;
	public static final Comparator<Transaction> SortByAmount = new Comparator<Transaction>(){

		@Override
		public int compare(Transaction arg0, Transaction arg1) {
			// TODO Auto-generated method stub
			if(arg0.amount < arg1.amount) return -1;
			if(arg0.amount > arg1.amount) return +1;
			return 0;
		}
	};
	public static final Comparator<Transaction> SortByWhen = new Comparator<Transaction>(){

		@Override
		public int compare(Transaction arg0, Transaction arg1) {
			// TODO Auto-generated method stub
			return arg0.when.compareTo(arg1.when);
		}
	};	
	public static final Comparator<Transaction> SortByWho = new Comparator<Transaction>(){

		@Override
		public int compare(Transaction arg0, Transaction arg1) {
			// TODO Auto-generated method stub
			return arg0.who.compareTo(arg1.who);
		}
	};	
	
	public Transaction()
	{
		this.who = StdString.generateRandomString(1, 3, 10)[0];
		this.when = new Date();
		this.amount = StdRandom.uniform(1.0,10000.0);
	}
	
	public Transaction(String who,Date when,double amount)
	{
		this.who = who;
		this.when = when;
		this.amount = amount;
	}
	public Transaction(String transition)
	{
		String [] a = transition.split("\\s+");
		who = a[0];
		when = new Date(a[1]);
		amount = Double.parseDouble(a[2]);
	}
	public String toString()
	{
		//return who+" "+when.toString()+" "+amount;
		return String.format("%-12s%-12s%-10.1f",who,when,amount);
	}
	public boolean equals(Transaction that)
	{
		if(that == this) 			return true;
		if(that == null)			return false;
		if(that.getClass() != this.getClass())		return false;
		return (this.amount == that.amount) && (this.who.equals(that.who) 
											&& this.when.equals(that.when));
	}
	@Override
	public int compareTo(Transaction that) {
		// TODO Auto-generated method stub
		if(this.amount > that.amount) return +1;
		if(this.amount < that.amount) return -1;
		return 0;
	}
	
	public int hashCode(){
		int hash = 17;
		hash = 31*hash + who.hashCode();
		hash = 31*hash + when.hashCode();
		hash = 31*hash + ((Double)amount).hashCode();
		return hash;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int N = 1000;
		Transaction [] transactions = new Transaction[N];
		for(int i = 0; i < N; i++)
		{
			transactions[i] = new Transaction();
		}
		Out o1,o2,o3;
		o1 = new Out("transactionsortbywho.txt");
		o2 = new Out("transactionsortbyamount.txt");
		o3 = new Out("transactionsortbywhen.txt");
		StopWatch timer = new StopWatch();
		Arrays.sort(transactions,Transaction.SortByAmount);
		for(int i = 0; i < transactions.length; i++)
		{
			o2.println(transactions[i]);
		}
		o2.close();
		Arrays.sort(transactions,Transaction.SortByWho);
		for(int i = 0; i < transactions.length; i++)
		{
			o1.println(transactions[i]);
		}
		o1.close();
		Arrays.sort(transactions,Transaction.SortByWhen);
		for(int i = 0; i < transactions.length; i++)
		{
			o3.println(transactions[i]);
		}
		o3.close();
		StdOut.println("Finish:"+timer.elapsedTime());
		
		
	}
}

package com.example.algs;

public class VisualAccumulator {
	private Accumulator a;
	public VisualAccumulator(int trials, double max)
	{
		a = new Accumulator();
		StdDraw.setXscale(0,trials);
		StdDraw.setYscale(0,max);
		StdDraw.setPenRadius(.005);
	}
	
	public void addDataValue(double val)
	{
		a.addDataValue(val);
		StdDraw.setPenColor(StdDraw.DARK_GRAY);
		StdDraw.point(a.getN(), val);
		StdDraw.setPenColor(StdDraw.RED);
		StdDraw.point(a.getN(), a.mean());
	}
	public String toString()
	{
		return a.toString();
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int T = 10000;
		VisualAccumulator va = new VisualAccumulator(T,1.0);
		for(int i = 0; i < T; i++)
		{
			va.addDataValue(StdRandom.random());
		}
		StdOut.println(va);
	}

}

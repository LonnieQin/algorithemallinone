package com.example.algs.collections;

import java.util.Iterator;

import com.example.algs.StdOut;
import com.example.algs.StdString;

public class BST<Key extends Comparable<Key>,Value>{
	private Node root;
	private class Node
	{
		Key key;
		Value value;
		Node left,right;
		public Node(Key key,Value val)
		{
			this.key = key;
			this.value = val;
		}
	}
	public BST() {
		// TODO Auto-generated constructor stu
	}
	
	public void put(Key key,Value value)
	{
		root = put(root,key,value);
	}
	
	private Node put(Node node,Key key,Value value)
	{
		if(node == null) return new Node(key,value);
		int cmp = key.compareTo(node.key);
		if		(cmp < 0) node.left = put(node.left,key,value);
		else if	(cmp > 0) node.right = put(node.right,key,value);
		else	node.value = value;
		return node;
	}
	
	public Value get(Key key)
	{
		return get(root,key);
	}


	private Value get(Node x, Key key) {
		// TODO Auto-generated method stub
		if(x == null) return null;
		int cmp = key.compareTo(x.key);
		if		(cmp < 0)	return get(x.left,key);
		else if	(cmp > 0)	return get(x.right,key);
		else				return x.value;
	}

	public boolean contains(Key key)
	{
		return (get(key) != null);
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BST<String,String> bst = new BST<String,String>();
		for(int i = 0; i < 100; i++)
		bst.put(StdString.generateRandomString(1, 3, 5)[0], StdString.generateRandomString(1, 3, 10)[0]);
		Queue<String> queue = bst.inOrder();
		while(!queue.isEmpty())
		{
			StdOut.println(queue.dequeue());
		}
	}

	public Stack<Key> inorder()
	{
		Stack<Key> stack = new Stack<Key>();
		inorder(stack,root);
		return stack;
	}

	private void inorder(Stack<Key> stack, Node root) {
		// TODO Auto-generated method stub
		if(root == null) return;
		inorder(stack,root.left);
		stack.push(root.key);
		inorder(stack,root.right);
	}
	
	public Queue<Key> frontOrder()
	{
		Queue<Key> queue = new Queue<Key>();
		frontOrder(queue,root);
		return queue;
	}

	private void frontOrder(Queue<Key> stack, Node root) {
		// TODO Auto-generated method stub
		if(root == null) return;
		stack.enqueue(root.key);
		frontOrder(stack,root.left);
		frontOrder(stack,root.right);
	}
	
	public Queue<Key> inOrder()
	{
		Queue<Key> queue = new Queue<Key>();
		frontOrder(queue,root);
		return queue;
	}

	private void inOrder(Queue<Key> stack, Node root) {
		// TODO Auto-generated method stub
		if(root == null) return;
		frontOrder(stack,root.left);
		stack.enqueue(root.key);
		frontOrder(stack,root.right);
	}
	
	
	public Stack<Key> backOrder()
	{
		Stack<Key> stack = new Stack<Key>();
		backOrder(stack,root);
		return stack;
	}

	private void backOrder(Stack<Key> stack, Node root) {
		// TODO Auto-generated method stub
		if(root == null) return;
		backOrder(stack,root.left);
		backOrder(stack,root.right);
		stack.push(root.key);
	}

}

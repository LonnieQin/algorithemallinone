package com.example.algs.collections;

import java.util.Iterator;
import java.util.NoSuchElementException;

import com.example.algs.StdOut;

public class Bag<Item> implements Iterable<Item> {
	private int N;
	private Node<Item> first;
	
	/*
	private static class Node<Item>{
		private Item item;
		private Node<Item> next;
	}
	*/
	public Bag() {
		// TODO Auto-generated constructor stub
		first = null;
		N = 0;
	}
	
	/**
	 * Is this bag empty?
	 * @return true if this bag is empty
	 */
	public boolean isEmpty(){
		return first == null;
	}
	
	/**
	 * Returns the number of items in this bag
	 * @return the number of items in this bag
	 */
	public int size(){
		return N;
	}

	/**
	 * Adds the item to this bag
	 * @param item the item to add to this bag
	 */
	public void add(Item item){
		Node<Item> oldfirst = first;
		first = new Node<Item>();
		first.item = item;
		first.next = oldfirst;
		N++;
	}
	
	
	
	@Override
	public Iterator<Item> iterator() {
		// TODO Auto-generated method stub
		return new ListIterator<Item>(first);
	}
	/*
	private class ListIterator<Item> implements Iterator<Item>
	{
		private Node<Item> current;
		
		public ListIterator(Node<Item> first){
			current = first;
		}
		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return current != null;
		}

		@Override
		public Item next() {
			// TODO Auto-generated method stub
			if (!hasNext()) throw new NoSuchElementException();
			Item item = current.item;
			current = current.next;
			return item;
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub
			throw new UnsupportedOperationException();
		}
		
	}
	*/
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bag<String> bag = new Bag<String>();
		int i = 0;
		while(i < 10)
		{
			String item = "String"+i;
			bag.add(item);
			i++;
		}
		StdOut.println("Size of bag = " + bag.size());
		for(String s:bag)
		{
			StdOut.println(s);
		}
	}

}

package com.example.algs.collections;

import com.example.algs.Array;
import com.example.algs.StdOut;
import com.example.algs.StdRandom;
import com.example.algs.sort.QuickSort;

public class BinarySearchST<Key extends Comparable<Key>, Value> {

	Key [] keys;
	Value [] values;
	int MaxN;
	int N;
	public BinarySearchST() {
		// TODO Auto-generated constructor stub
		N = 0;
		MaxN = 1;
		keys = (Key[])new Comparable[MaxN];
		values = (Value[])new Object[MaxN];
	}	
	
	public Key min()
	{	return keys[0];}
	public Key max()
	{	return keys[N-1];}
	public Key select(int k)
	{	return keys[k];}
	public Key ceilling(Key key)
	{	
		int i = rank(key);
		return keys[i];
	}
	
	public Key floor(Key key)
	{
		return null;
	}
	
	/***
	 * resize key
	 */
	private void resizeKey(int n)
	{
		Key [] tempKeys = (Key[])new Comparable[n];
		for(int i = 0; i < keys.length; i++)
			tempKeys[i] = keys[i];
		keys = tempKeys;
	}
	
	/***
	 * resize value
	 */
	private void resizeValue(int n)
	{
		Value [] tempValues = (Value[])new Object[n];
		for(int i = 0; i < values.length; i++)
			tempValues[i] = values[i];
		values = tempValues;
	}
	
	/**
	 * put key-value pair into the table
	 * @param key
	 * @param value
	 */
	public void put(Key key, Value value){
		int i = rank(key);
		if( i < N && keys[i].compareTo(key) == 0)
		{	values[i] = value; return;}
		if(N == MaxN) 
		{
			MaxN = 2*N;
			resizeKey(MaxN);
			resizeValue(MaxN);
		}
		for(int j = N; j > i; j--)
		{	keys[j] = keys[j-1]; values[j] = values[j-1];}
		keys[i] = key; values[i] = value;
		N++;
	}
	
	public int rank(Key key)
	{
		int lo = 0,hi = N-1;
		while(lo <= hi)
		{
			int mid = lo+(hi-lo)/2;
			int cmp = key.compareTo(keys[mid]);
			if		(cmp < 0)	hi = mid - 1;
			else if	(cmp > 0)	lo = mid + 1;
			else return mid;
		}
		return lo;
	}
	
	private static int search(Comparable key,Comparable [] a)
	{
		int lo = 0,hi = a.length;
		while(lo <= hi)
		{
			int mid = lo+(hi-lo)/2;
			if(key.compareTo(a[mid]) > 0)
			{
				lo = mid;
			}
			else if(key.compareTo(a[mid]) < 0)
			{
				hi = mid;
			}
			else
				return mid;
		}
		return -1;
	}

	/**
	 * get value paired with key
	 * @param key key
	 * @return value paired with key
	 */
	Value get(Key key){
		if(isEmpty())
			return null;
		int index = rank(key);
		if(index < N && keys[index].compareTo(key) == 0) 	return values[index];
		else												return null;
	}
	
	/**
	 * is there a value paired with key?
	 * @param key 
	 * @return
	 */
	boolean contains(Key key){
		return search(key,keys) != -1;
	}
	
	public void delete(Key key)
	{	
		int index = search(key,keys);
		if(index != -1)	
		{
			values[index] = null;
		}
	}
	
	boolean isEmpty(){return N==0;}
	int size(){ return N;}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BinarySearchST<String,Integer> st = new BinarySearchST<String,Integer>();
		for(int i = 0; i < 500; i++)
			st.put("key"+i, i);
		for(int i = 0; i < 500; i++)
			StdOut.print(" "+st.get("key"+i));

		
	}

}
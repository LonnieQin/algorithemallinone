package com.example.algs.collections;

import java.util.Iterator;

import com.example.algs.StdOut;

public class FixedStack<Item>  implements Iterable<Item>{
	Node<Item> first;
	private int N;
	private final int size;
	public FixedStack(int size) {
		// TODO Auto-generated constructor stub
		this.size = size;
		N = 0;
	}
	public int size()
	{
		return N;
	}
	public boolean isEmpty()
	{
		return N == 0;
	}
	public void push(Item item)
	{
		if(!isFull())
		{
			Node<Item> oldfirst = first;
			first = new Node<Item>();
			first.item = item;
			if(oldfirst != null)
			{
				first.next = oldfirst;
			}
			N++;
		}
	}
	public boolean isFull()
	{
		return (N >= size);
	}
	public Item pop()
	{
		Item i = null;
		if(first != null)
		{
			i = first.item;
			if(first.next != null)
				first = first.next;
			N--;
		}
		return i;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FixedStack<Integer> stack = new FixedStack<Integer>(100);
		for(int i = 0; i < 202; i++)
		{
			stack.push(i*i+i);
			StdOut.println(i+" isfull:"+stack.isFull());
		}
		while(!stack.isEmpty())
		{
			StdOut.println(stack.pop());
		}
	}

	@Override
	public Iterator<Item> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

}

package com.example.algs.collections;

import java.util.Iterator;

import com.example.algs.StdOut;

public class HashTable<Item>{
	public static class HashNode<Item>
	{
		Item item;
		String key;
	}
	public HashNode<Item> [] nodes;
	int elemCount;
	public void resize(int max)
	{
		System.out.println("RESIZE");
		HashNode[] temp = new HashNode[max];
		for(int i = 0; i < nodes.length; i++)
		{
			if(nodes[i] == null) continue;
			int index = getEmptyPos(nodes[i].key);
			temp[index] = nodes[i];
		}
		nodes = temp;
	}
	public HashTable() {
		// TODO Auto-generated constructor stub
		nodes = new HashNode[4];
		elemCount = 0;
	}
	public long hash(String s)
	{
		int sum = 0;
		for(int i = 0; i < s.length(); i++)
		{
			sum += s.charAt(i);
		}
		long h = (24179*sum+24181);
		//StdOut.println("hash("+s+")="+h);
		return h;
	}
	public int getEmptyPos(String key)
	{
		int index  = (int)(hash(key)%nodes.length);
		int count = 0;
		while(count <= nodes.length)
		{
			if(nodes[index] == null)
				return index;
			else
			{
				index++;
				count++;
				index %= nodes.length;
			}
		}
		return 0;
	}
	public int rehash(String s)
	{
		return 0;
	}
	public Item get(String key)
	{
		int index  = (int)(hash(key)%nodes.length);
		int count = 0;
		while(count <= nodes.length)
		{
			if(nodes[index] != null && nodes[index].key.equals(key))
			{
				return nodes[index].item;
			}
			++count;
			++index;
			index %= nodes.length;
		}
		return null;
	}
	public void set(String key,Item val)
	{
		HashNode<Item> node = new HashNode<Item>();
		node.key = key;
		node.item = val;
		++elemCount;
		if(elemCount >= nodes.length)
		{
			resize(nodes.length*2);
		}
		int index = getEmptyPos(key);
		nodes[index] = node;
	}
	public void remove(String key)
	{
		int index  = (int)(hash(key)%nodes.length);
		int count = 0;
		while(count <= nodes.length)
		{
			if(nodes[index] != null && nodes[index].key.equals(key))
			{
				StdOut.println("Remove "+nodes[index].key);
				nodes[index] = null;
				elemCount --;
				break;
			}
			++count;
			
			++index;
			index %= nodes.length;
		}
	}
	public static void main(String[] args) {
		HashTable<String> hashTable = new HashTable<String>();
		for(int i = 0; i < 4; i++)
		{
			hashTable.set("key"+i, "val"+i);
		}
		for(int i = 0; i < 4; i++)
		{
			StdOut.println(hashTable.get("key"+i));
		}
	}

}

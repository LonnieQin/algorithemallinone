package com.example.algs.collections;

import java.util.NoSuchElementException;

public class IndexMinPQ<Key extends Comparable<Key>> {
	
	private int NMAX;		//maximun nubmer of elements on PQ
	private int N;			//number of elements on PQ
	private int	[] pq;		//binary heap using 1-based indexing
	private int [] qp;		//inverse of pq - qp[pq[i]] = i;
	private Key [] keys;	//keys[i] = priority of i
	/**
	 * create indexed priority queue with indices 0,1,...,N-1
	 * @param N
	 */
	public IndexMinPQ(int NMAX) {
		// TODO Auto-generated constructor stub
		if (NMAX < 0)throw new IllegalArgumentException();
		this.NMAX = NMAX;
		keys = (Key[])new Comparable[NMAX + 1];	//make this of lenght NMAX
		pq = new int[NMAX+1];
		qp = new int[NMAX+1];
		for	(int i = 0; i <= NMAX; i++)qp[i] = -1;
	}
	
	/**
	 * associate key with index i
	 * @param i
	 * @param key
	 */
	public void insert(int i,Key key)
	{
		if	(i < 0 || i >= NMAX)	throw new IndexOutOfBoundsException();
		if(contains(i))				throw new IllegalArgumentException("index is already in the priority queue");
		N++;
		qp[i] = N;
		pq[N] = i;
		keys[i] = key;
		swim(N);
	}
	
	public void change(int i,Key key)
	{
		changeKey(i,key);
	}
	
	public void changeKey(int i,Key key)
	{
		keys[i] = key;
		swim(qp[i]);
		swim(qp[i]);
	}
	
	private void swim(int k) {
		// TODO Auto-generated method stub
		while (k > 1 && greater(k/2,k)){
			exch(k,k/2);
			k = k/2;
		}
	}

	private boolean greater(int i, int j) {
		// TODO Auto-generated method stub
		return keys[pq[i]].compareTo(keys[pq[j]]) > 0;
	}

	/**
	 * decrease key associate with i
	 * @param i
	 * @param key
	 */
	public void decreaseKey(int i,Key key)
	{
		if	(i < 0 || i >= NMAX)	throw new IndexOutOfBoundsException();
		if	(!contains(i))			throw new NoSuchElementException("index is not in the priority queue");
		if	(keys[i].compareTo(key) <= 0) throw new IllegalArgumentException("Calling decreaseKey() with given argument wouldnot strictly decrease the key");
		keys[i] = key;
		swim(qp[i]);
	}
	
	/**
	 * is i an index on the priority queue?
	 * @param i
	 * @return
	 */
	public boolean contains(int i)
	{
		if	(i < 0 || i >= NMAX)throw new IndexOutOfBoundsException();
		return qp[i] != -1;
	}
	
	/**
	 * remove a minimal key and returns its associated index
	 * @return
	 */
	public int delMin()
	{
		if	(N==0)	throw new NoSuchElementException("Priority queue underflow");
		int min = pq[1];
		exch(1,N--);
		sink(1);
		qp[min] = -1;
		keys[pq[N+1]] = null;
		pq[N+1] = -1;
		return min;
	}
	
	
	private void sink(int k) {
		// TODO Auto-generated method stub
		while(2*k <= N){
			int j = 2*k;
			if(j < N && greater(j,j+1))j++;
			if(!greater(k,j))break;
			exch(k,j);
			k = j;
		}
	}
	
	public void increaseKey(int i,Key key)
	{
		if	(i < 0 || i >= NMAX)new IndexOutOfBoundsException();
		if	(!contains(i))			throw new NoSuchElementException("index is not in the priority queue");
		if	(keys[i].compareTo(key) >= 0) throw new IllegalArgumentException("Calling increaseKey() with given argument wouldnot strictly increase the key");
		keys[i] = key;
		sink(qp[i]);
	}

	private void exch(int i, int j) {
		// TODO Auto-generated method stub
		int swap = pq[i];pq[i] = pq[j]; pq[j] = swap;
		qp[pq[i]] = i; qp[pq[j]] = j;
	}

	/**
	 * is the priority queue empty?
	 * @return
	 */
	public boolean isEmpty()
	{
		return N==0;
	}
	
	/**
	 * number of keys in the priority queue
	 * @return
	 */
	public int size()
	{
		return N;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

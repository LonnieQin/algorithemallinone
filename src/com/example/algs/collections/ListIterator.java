package com.example.algs.collections;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ListIterator<Item> implements Iterator<Item> {
	private Node<Item> current;
	public ListIterator(Node<Item> node) {
		// TODO Auto-generated constructor stub
		current = node;
	}
	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return current != null;
	}

	@Override
	public Item next() {
		// TODO Auto-generated method stub
		if (!hasNext()) throw new NoSuchElementException();
		Item item = current.item;
		current = current.next;
		return item;
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

}

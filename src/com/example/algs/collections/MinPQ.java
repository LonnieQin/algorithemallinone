package com.example.algs.collections;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import com.example.algs.StdOut;
import com.example.algs.StdRandom;

public class MinPQ<Key>  implements Iterable<Key> {

	public MinPQ() {
		// TODO Auto-generated constructor stub
		this(1);
	}
	
	public MinPQ(int initCapacity){
		pq  = (Key[])new Object[initCapacity+1];
		N = 0;
	}
	
	public MinPQ(int initCapacity,Comparator<Key> comparator)
	{
		this.comparator = comparator;
		pq = (Key[]) new Object[initCapacity+1];
		N = 0;
	}
	
	public MinPQ(Comparator<Key> comparator){this(1,comparator);}
	
	public MinPQ(Key[] keys){
		N = keys.length;
		pq = (Key[])new Object[keys.length+1];
		for(int i = 0; i < N; i++)
			pq[i+1] = keys[i];
		for(int k = N/2; k >=1; k--)
			sink(k);
		assert isMinHeap();
		
	}
	
	private boolean isMinHeap() {
		// TODO Auto-generated method stub
		return isMinHeap(1);
	}
	private boolean isMinHeap(int k){
		if(k > N) return true;
		int left = 2*k,right = 2*k+1;
		if(left <= N && larger(k,left)) return false;
		if(right <= N && larger(k,right)) return false;
		return isMinHeap(left) && isMinHeap(right);
	}

	private Key[] pq;	//heap-ordered complete binary tree
	private int N = 0;	//pq[1..N] with pq[0] unused
	private Comparator<Key> comparator;
	
	public boolean isEmpty()
	{
		return N==0;
	}
	/**
	 * Return the number of keys
	 * @return
	 */
	public int size()
	{	return N;	}
	
	/**
	 * a smallest key on the priority queue
	 * @return
	 */
	public Key min(){
		if(isEmpty())throw new NoSuchElementException("Priority Queue is Empty!");
		return pq[1];
	}
	public void insert(Key v)
	{
		if(N == pq.length - 1) resize(2*pq.length);
		pq[++N] = v;
		swim(N);
	}
	
	private void resize(int n) {
		// TODO Auto-generated method stub
		assert n > N;
		Key [] newPq = (Key[]) new Object[n];
		for(int i = 1; i <= N;i++){
			newPq[i] = pq[i];
		}
		pq = newPq;
	}
	public Key delMin()
	{
		if(isEmpty())throw new NoSuchElementException("Priority queue underflow");
		exch(1,N);
		Key min = pq[N--];	//Retrieve max key from top
		sink(1);
		pq[N+1] = null;		//Avoid loitering.
		if((N > 0) && (N == (pq.length-1)/4)) resize(pq.length/2);
		assert isMinHeap();
		return min;
	}
	private void sink(int k) {
		// TODO Auto-generated method stub
		while(2*k <= N)
		{
			int j = 2*k;
			if(j < N && larger(j,j+1)) j++;
			if(!larger(k,j)) break;
			exch(k,j);
			k = j;
		}
	}
	private boolean larger(int i, int j) {
		// TODO Auto-generated method stub
		if(comparator == null){
			return ((Comparable<Key>)pq[i]).compareTo(pq[j]) > 0;
		}
		else{
			return comparator.compare(pq[i], pq[j]) > 0;
		}
	}
	private void exch(int i, int j) {
		// TODO Auto-generated method stub
		Key t = pq[i];
		pq[i] = pq[j];
		pq[j] = t;
	}
	private void swim(int k) {
		// TODO Auto-generated method stub
		while (k > 1 && larger(k/2,k))
		{
			exch(k/2,k);
			k = k / 2;
		}
	}
	
	public static void main(String [] args)
	{
		int N = 10000;
		MinPQ<Integer> pq = new MinPQ<Integer>();
		for(int i = 0; i < N; i++)
		{
			int n = StdRandom.uniform(500);
			pq.insert(n);
			StdOut.print(n+" ");
		}
		StdOut.println();
		for(int i:pq)
			StdOut.print(i+" ");
		while(pq.size() != 0)
		{
			StdOut.print(pq.delMin()+" ");
		}
		StdOut.println();
	}
	 public Iterator<Key> iterator() { return new HeapIterator(); }

	    private class HeapIterator implements Iterator<Key> {
	        // create a new pq
	        private MinPQ<Key> copy;

	        // add all items to copy of heap
	        // takes linear time since already in heap order so no keys move
	        public HeapIterator() {
	            if (comparator == null) copy = new MinPQ<Key>(size());
	            else                    copy = new MinPQ<Key>(size(), comparator);
	            for (int i = 1; i <= N; i++)
	                copy.insert(pq[i]);
	        }

	        public boolean hasNext()  { return !copy.isEmpty();                     }
	        public void remove()      { throw new UnsupportedOperationException();  }

	        public Key next() {
	            if (!hasNext()) throw new NoSuchElementException();
	            return copy.delMin();
	        }
	    }	
}

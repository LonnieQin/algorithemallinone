package com.example.algs.collections;

public class Node<Item> {
	Item item;
	Node<Item> next;
}

package com.example.algs.collections;

import java.util.Iterator;

import com.example.algs.Date;
import com.example.algs.In;
import com.example.algs.Out;
import com.example.algs.StdOut;
import com.example.algs.StdRandom;

public class Queue<Item> implements Iterable<Item> {

	private Node<Item> first;
	private Node<Item> last;
	private	int N;
	
	public boolean isEmpty(){	return first == null;  }
	public int size()		{	return N;}
	public void enqueue(Item item)
	{
		Node<Item> oldlast = last;
		last = new Node<Item>();
		last.item = item;
		last.next = null;
		if(isEmpty()) 	first = last;
		else			oldlast.next = last;
		N++;
	}
	public Item dequeue()
	{
		Item item = first.item;
		first = first.next;
		if(isEmpty()) last = null;
		N--;
		return item;
	}
	public Queue() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Iterator<Item> iterator() {
		// TODO Auto-generated method stub
		return new ListIterator<Item>(first);
	}
	public static int [] readInts(String name)
	{
		In in = new In(name);
		Queue<Integer> q = new Queue<Integer>();
		while(!in.isEmpty())
		{
			q.enqueue(in.readInt());
		}
		
		int N = q.size();
		int [] a = new int[N];
		for(int i = 0; i < N; i++)
			a[i] = q.dequeue();
		return a;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Queue<String> queue = new Queue<String>();
		for(int i = 0; i < 100; i++)
		{
			queue.enqueue("String"+i);
		}
		for(String s:queue)
		{
			System.out.println(s);
		}
		while(!queue.isEmpty())
		{
			StdOut.println(queue.dequeue());
		}
		Queue<Date> dateQueue = new Queue<Date>();
		for(int i = 0; i < 100; i++)
		{
			dateQueue.enqueue(new Date(StdRandom.uniform(1, 13),StdRandom.uniform(1, 32),StdRandom.uniform(2000, 2080)));
		}
		for(Date d:dateQueue)
		{
			StdOut.println(d);
		}
		
		Queue<Integer> intQueue = new Queue<Integer>();
		for(int i = 0; i < 10;i++)
		{
			intQueue.enqueue(i);
		}
		for(int i:intQueue)
		{
			System.out.println(i);
		}
		Out o = new Out("ints.txt");
		for(int i = 0; i < 1000; i++)
		{
			o.print(StdRandom.uniform(1000)+" ");
		}
		o.close();
		int [] ints = Queue.readInts("ints.txt");
		for(int i : ints)
		{
			StdOut.print(i+" ");
		}
	}

}

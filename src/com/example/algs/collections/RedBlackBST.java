package com.example.algs.collections;

import java.util.Arrays;

import com.example.III.Word;
import com.example.algs.In;
import com.example.algs.Out;
import com.example.algs.StdOut;
import com.example.algs.StopWatch;

public class RedBlackBST<Key extends Comparable<Key>,Value> {

	private static boolean RED = true;
	private static boolean BLACK = false;
	private Node root;
	private class Node{
		private Key key;
		private Value val;
		private Node left,right;
		private boolean color;
		private int N;
		public Node(Key key,Value val,boolean color,int N)
		{
			this.key = key;
			this.val = val;
			this.color = color;
			this.N = N;
		}
	}
	
	public int size()
	{
		return size(root);
	}
	public int size(Node x)
	{
		if(x == null)return 0;
		return x.N;
	}
	
	private boolean isRed(Node x)
	{
		if(x == null) return false;
		return x.color == RED;
	}
	
	public boolean contains(Key key)
	{
		return get(key) != null;
	}


	public void put(Key key,Value val)
	{
		root = put(root,key,val);
		root.color = BLACK;
	}
	private Node put(Node h,Key key,Value val)
	{
		if(h == null) return new Node(key,val,RED,1);
		int cmp = key.compareTo(h.key);
		if		(cmp < 0)   h.left = put(h.left,key,val);
		else if (cmp > 0) 	h.right = put(h.right,key,val);
		else if (cmp == 0)	h.val = val;
		
		if(isRed(h.right) && !isRed(h.left)) 	h = rotateLeft(h);
		if(isRed(h.left) && isRed(h.left.left))	h = rotateRight(h);
		if(isRed(h.left) && isRed(h.right))		flipColors(h);
		h.N = size(h.left)+size(h.right)+1;
		return h;
	}
	
	public Value get(Key key)
	{
		Node x = root;
		return get(root,key);
	}
	public Value get(Node x,Key key)
	{
		while(x != null)
		{
			int cmp = key.compareTo(x.key);
			if		(cmp < 0)	x = x.left;
			else if	(cmp > 0)	x = x.right;
			else 				return x.val;
		}
		return null;
	}
	
	private Node rotateLeft(Node h)
	{
		//assert isRed(h.right);
		Node x = h.right;
		h.right = x.left;
		x.left = h;
		x.color = x.left.color;
		x.left.color = RED;
		x.N = h.N;
		h.N = size(h.left) + size(h.right) + 1;
		return x;
	}
	private Node rotateRight(Node h)
	{
		//assert isRed(h.left);
		Node x = h.left;
		h.left = x.right;
		x.right = h;
		x.color = h.color;
		h.color = RED;
		x.N = h.N;
		h.N = size(h.left)+size(h.right)+1;
		return x;
	}
	
	private void flipColors(Node h)
	{
//		assert !isRed(h);
//		assert isRed(h.left);
//		assert isRed(h.right);
		h.color = RED;
		h.left.color = !h.left.color;
		h.right.color = !h.right.color;
	}
	
	public boolean isEmpty()
	{
		return size() == 0;
	}
	
	
	public Key min()
	{
		if(isEmpty()) return null;
		return min(root).key;
	}
	public Node min(Node x)
	{
		if(x.left == null)
			return x;
		return min(x.left);
	}
	public Key max()
	{
		if(isEmpty()) return null;
		return max(root).key;
	}
	public Node max(Node x)
	{
		if(x.right == null)
			return x;
		return max(x.right);
	}
	
	public Iterable<Key> keys()
	{
		Queue<Key> q = new Queue<Key>();
		keys(root,q,min(),max());
		return q;
	}
	
	public void keys(Node x,Queue<Key> queue,Key lo,Key hi)
	{
		if	(x == null)	return;
		int cmplo = lo.compareTo(x.key);
		int cmphi = hi.compareTo(x.key);
		if(cmplo < 0) keys(x.left,queue,lo,hi);
		if(cmplo <= 0 && cmphi >= 0)	queue.enqueue(x.key);
		if(cmphi > 0) keys(x.right,queue,lo,hi);
	}

	public static void main(String[] args) {
		for(int N = 125; N < 320000; N*=2)
		{
			RedBlackBST<String,Integer> st = new RedBlackBST<String,Integer>();
			//BST<String,Integer> st = new BST<String,Integer>();
			In in = new In("world192.txt");
			double prev = 0;
			double present = 0;
			StopWatch timer = new StopWatch();
			for(int i = 0; i < N && !in.isEmpty(); i++)
			{	
				String s = in.readString();
				StringBuilder sb = new StringBuilder();
				for(int j = 0; j < s.length(); j++)
				{
					char c = s.charAt(j);
					if(c >= 'a' && c<= 'z' || c >= 'A' && c<= 'Z')
						sb.append(c);
				}
				String word = sb.toString().toLowerCase();
				if(word.length() < 2) continue;
				if(!st.contains(word))			st.put(word, 1);
				else							st.put(word, st.get(word)+1);
			}
			Word [] words = new Word[st.size()];
			Queue<String> keys = (Queue<String>) st.keys();
			int i = 0;
			while(!keys.isEmpty())
			{
				Word word = new Word();
				word.word = keys.dequeue();
				word.frequence = st.get(word.word);
				words[i++] = word;
			}
			Arrays.sort(words,Word.FrenquenceDecrease);
			Out out = new Out("wordfrequent"+N+".txt");
			for(Word word:words)
			{
				out.println(word.word+" "+word.frequence);
			}
			double storetime = timer.elapsedTime();
			if(N == 125) prev = storetime;
			present = storetime;
			if(N > 125)
				StdOut.println("store "+N+" time:"+present+" growth rate:"+present/prev);
			prev = present;
		}
		
	}

}

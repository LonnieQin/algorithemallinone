package com.example.algs.collections;

import java.util.Iterator;

import com.example.algs.StdOut;

public class ResizingArrayStack<Item> implements Iterable<Item> {
	
	private Item[] a = (Item[]) new Object[1];
	private int N = 0;
	public boolean isEmpty(){return N == 0;}
	public int size()		{return N;}
	
	private void resize(int max)
	{
		Item [] temp = (Item[]) new Object[max];
		for	(int i = 0; i < N; i++)
			temp[i] = a[i];
		a = temp;
		StdOut.println("RESICE");
	}
	
	public void push(Item item)
	{
		if(N == a.length)	resize(2*a.length);
		a[N++] = item;
	}
	public Item pop()
	{
		Item item = a[--N];
		a[N] = null;
		if(N > 0 && N == a.length/4) resize(a.length/2);
		return item;
	}
	@Override
	public Iterator<Item> iterator() {
		// TODO Auto-generated method stub
		return new ReverseArrayIterator();
	}
	
	private class ReverseArrayIterator implements Iterator<Item> {
		
		private int i = N;
		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return i > 0;
		}

		@Override
		public Item next() {
			// TODO Auto-generated method stub
			return a[--i];
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub
			
		}

	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ResizingArrayStack<String> stack = new ResizingArrayStack<String>();
		String sentence = "To be or not to be That is a question I love you Mom So do i YJL I love You more than anything";
		String [] strs = sentence.split(" ");
		for(String s:strs)
		{
			stack.push(s);
		}
		for(String s:stack)
		{
			StdOut.print(s+" ");
		}
		ResizingArrayStack<Double> [] stacks = (ResizingArrayStack<Double>[]) new ResizingArrayStack[100];
	}

}

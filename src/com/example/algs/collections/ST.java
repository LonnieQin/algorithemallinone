package com.example.algs.collections;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;

import com.example.algs.StdOut;
import com.example.algs.StdRandom;
import com.example.algs.StdString;

public class ST<Key extends Comparable<Key>,Value> implements Iterable<Key> {
	private TreeMap<Key,Value> st = new TreeMap<Key,Value>();
	public ST() {
		// TODO Auto-generated constructor stub
		
	}

	public void put(Key key,Value val)
	{
		if(val == null) st.remove(key);
		else			st.put(key, val);
	}
	public Value get(Key key)			{return st.get(key);}
	public Value remove(Key key)		{ return st.remove(key);}
	public  boolean contains(Key key)	{return st.containsKey(key);}
	public Iterator<Key> iterator()		{return st.keySet().iterator();}
	public int size()					{return st.size();}
	public Set<Key> keys() {
		// TODO Auto-generated method stub
		return st.keySet();
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ST<String,Integer> st = new ST<String,Integer>();
		for(int i = 0; i < 1000; i++)
			st.put(StdString.generateRandomString(1, 3, 5)[0], StdRandom.uniform(1000));
		for(String s:st)
			StdOut.println(s+ " "+st.get(s)+" ");
	}



}

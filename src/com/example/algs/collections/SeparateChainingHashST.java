package com.example.algs.collections;

import com.example.algs.StdOut;

public class SeparateChainingHashST<Key,Value> {
	private int N;		//number of key-value pairs
	private int M;		//hash table size
	private SequentialSearchST<Key,Value>[]	st;
	public SeparateChainingHashST() {
		// TODO Auto-generated constructor stub
		this(997);
	}
	public SeparateChainingHashST(int M){
		this.M = M;
		N = 0;
		st = (SequentialSearchST<Key,Value>[])new SequentialSearchST[M];
		for(int i = 0; i < M; i++)
			st[i] = new SequentialSearchST<Key,Value>();
	}
	public int size()
	{
		return N;
	}
	private int hash(Key key)
	{	return (key.hashCode() & 0x7fffffff) % M;}
	
	public Value get(Key key)
	{	
		return st[hash(key)].get(key);
	}
	public void put(Key key,Value value)
	{
		if(this.get(key) == null)N++;
		st[hash(key)].put(key, value);
	}
	public Queue<Key> keys()
	{
		Queue<Key> q = new Queue<Key>();
		for(int i = 0; i < M;i++)
		{
			Queue<Key> qq = st[i].keys();
			while(!qq.isEmpty())
			{
				q.enqueue(qq.dequeue());
			}
		}
		return q;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SeparateChainingHashST<Integer,Integer> st = new SeparateChainingHashST<Integer,Integer>();
		for(int i = 0; i < 100; i++)
		{
			st.put(i, 100-i);
		}
		Queue<Integer> q = st.keys();
		while(!q.isEmpty())
		{
			int t = q.dequeue();
			StdOut.println(t+" "+st.get(t));
		}
		StdOut.println("Size:"+st.size());
	}

}

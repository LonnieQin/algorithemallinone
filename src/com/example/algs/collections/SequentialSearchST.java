package com.example.algs.collections;

import com.example.algs.StdOut;

public class SequentialSearchST<Key,Value> {

	Node root;
	private class Node
	{
		Key key;
		Value value;
		Node next;
		Node prev;
		public Node(Key key,Value value)
		{
			this.key = key;
			this.value = value;
			StdOut.println("new Node("+this.key+","+this.value+")");
		}
	}
	
	public void put(Key key,Value value)
	{
		if(root == null)
		{
			root = new Node(key,value);return;
		}
		Node x = root;
		Node prev = null;
		while(x != null)
		{
			prev = x;
			x = x.next;
		}
		x = new Node(key,value);
		prev.next = x;
		x.prev = prev;
	}
	
	public void delete(Key key)
	{
		Node x = root;
		while(x != null)
		{
			if(x.key.equals(key))	break;
			x = x.next;
		}
		if(x == null) return;
		if(x.equals(root))
		{
			root = x.next;
			return;
		}
		if(x != null){
		Node prev = x.prev;
		prev.next = x.next;
		}
	}
	public Value get(Key key)
	{
		Node x = root;
		while(x != null)
		{
			if(x.key.equals(key))	return x.value;
			x = x.next;
		}
		return null;
	}
	
	public Queue<Key> keys()
	{
		Queue<Key> q = new Queue<Key>();
		Node x = root;
		while(x != null)
		{
			q.enqueue(x.key);
			x = x.next;
		}
		return q;
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SequentialSearchST<String,String> st = new SequentialSearchST<String,String>();
		st.put("A", "A");
		st.put("B", "B");
		st.put("C", "C");
		st.put("D", "D");
		st.put("E", "E");
		Queue<String> q = st.keys();
		while(!q.isEmpty())
		{
			String key = q.dequeue();
			StdOut.println(key+" "+st.get(key));
		}
		st.delete("A");
		q = st.keys();
		while(!q.isEmpty())
		{
			String key = q.dequeue();
			StdOut.println(key+" "+st.get(key));
		}
	}

}

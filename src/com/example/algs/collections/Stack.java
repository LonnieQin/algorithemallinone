package com.example.algs.collections;

import java.util.Iterator;

public class Stack<Item> implements Iterable<Item> {
	private Node<Item> first;
	private int N;
	public Stack() {
		// TODO Auto-generated constructor stub
	}
	public boolean isEmpty(){	return first == null;}
	public int size()		{	return N;}
	
	public void push(Item item)
	{
		Node<Item> oldFirst = first;
		first = new Node<Item>();
		first.item = item;
		first.next = oldFirst;
		N++;
	}
	public Item pop()
	{
		Item item = first.item;
		first = first.next;
		N--;
		return item;
	}
	
	public Item peek()
	{
		if(first != null)
		return first.item;
		return null;
	}
	@Override
	public Iterator<Item> iterator() {
		// TODO Auto-generated method stub
		return new ListIterator<Item>(first);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Stack<String> stack = new Stack();
		for(int i = 0; i < 100; i++)
		{
			stack.push("stack"+i);
		}
		for(String s:stack)
		{
			System.out.println(s);
		}
		while(!stack.isEmpty())
		{
			System.out.println(stack.pop());
		}
	}

}

package com.example.algs.graph;

import com.example.algs.StdOut;
import com.example.algs.collections.Stack;

public class AcycliscLP {
	private DirectedEdge [] edgeTo;
	private double [] distTo;
	public AcycliscLP(EdgeWeightedDigraph G,int s) {
		// TODO Auto-generated constructor stub
		EdgeWeightedDigraph G2 = new EdgeWeightedDigraph(G.V());
		for(DirectedEdge e:G.edges())
			G2.addEdge(new DirectedEdge(e.from(),e.to(),-e.weight()));
		edgeTo = new DirectedEdge[G.V()];
		distTo = new double[G.V()];
		for	(int v = 0; v < G2.V(); v++)
			distTo[v] = Double.POSITIVE_INFINITY;
		distTo[s] = 0.0;
		
		Topological top = new Topological(G2);
		StdOut.println(top.order());
		for	(int v:top.order())
			relax(G,v);
	}

	private void relax(EdgeWeightedDigraph G, int v)
	{
		for	(DirectedEdge e:G.adj(v))
		{
			int w = e.to();
			if	(distTo[w] > distTo[v]+e.weight())
			{
				distTo[w] = distTo[v] + e.weight();
				edgeTo[w] = e;
			}
		}
	}
	public double distTo(int v)
	{
		return distTo[v];
	}
	public boolean hasPathTo(int v)
	{
		return distTo[v] != Double.POSITIVE_INFINITY;
	}
	
	public Iterable<DirectedEdge> pathTo(int v)
	{
		Stack<DirectedEdge> path = new Stack<DirectedEdge>();
		for(DirectedEdge e = edgeTo[v]; e!= null; e = edgeTo[e.from()])
			path.push(e);
		return path;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

package com.example.algs.graph;
import com.example.algs.In;
import com.example.algs.StdOut;
import com.example.algs.collections.Stack;
import com.example.algs.graph.DirectedEdge;
import com.example.algs.graph.EdgeWeightedDigraph;
import com.example.algs.graph.Topological;


public class AcycliscSP {

	private DirectedEdge [] edgeTo;
	private double [] distTo;
	public AcycliscSP(EdgeWeightedDigraph G,int s) {
		// TODO Auto-generated constructor stub
		edgeTo = new DirectedEdge[G.V()];
		distTo = new double[G.V()];
		
		for	(int v = 0; v < G.V(); v++)
			distTo[v] = Double.POSITIVE_INFINITY;
		distTo[s] = 0.0;
		
		Topological top = new Topological(G);
		StdOut.println(top.order());
		for	(int v:top.order())
			relax(G,v);
	}

	private void relax(EdgeWeightedDigraph G, int v)
	{
		for	(DirectedEdge e:G.adj(v))
		{
			int w = e.to();
			if	(distTo[w] > distTo[v]+e.weight())
			{
				distTo[w] = distTo[v] + e.weight();
				edgeTo[w] = e;
			}
		}
	}
	public double distTo(int v)
	{
		return distTo[v];
	}
	public boolean hasPathTo(int v)
	{
		return distTo[v] < Double.POSITIVE_INFINITY;
	}
	
	public Iterable<DirectedEdge> pathTo(int v)
	{
		Stack<DirectedEdge> path = new Stack<DirectedEdge>();
		for(DirectedEdge e = edgeTo[v]; e!= null; e = edgeTo[e.from()])
			path.push(e);
		return path;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EdgeWeightedDigraph G = new EdgeWeightedDigraph(new In("tinyEWDAG.txt"));
		for(int w = 0; w < G.V();w++){
			AcycliscSP sp = new AcycliscSP(G,w);
			for(int v = 0; v < G.V();v++){
				if(v == w) continue;
				StringBuilder sb = new StringBuilder();
				sb.append(w+" to "+v+":");
				for(DirectedEdge e:sp.pathTo(v)){
					sb.append(e.from()+"--"+e.weight()+"-->");
				}
				if(sp.distTo(v) != Double.POSITIVE_INFINITY)
					sb.append(v+" "+sp.distTo(v)+"\n");
				else
					sb.append("unreacheable!");
				StdOut.println(sb.toString());
			}
		}
		EdgeWeightedDigraph G2 = new EdgeWeightedDigraph(G.V());
		for(DirectedEdge e:G.edges()){
			DirectedEdge e2 = new DirectedEdge(e.from(),e.to(),-e.weight());
			G2.addEdge(e2);
		}
		for(int w = 0; w < G.V();w++){
			AcycliscSP sp = new AcycliscSP(G2,w);
			for(int v = 0; v < G.V();v++){
				if(v == w) continue;
				StringBuilder sb = new StringBuilder();
				sb.append(w+" to "+v+":");
				for(DirectedEdge e:sp.pathTo(v)){
					sb.append(e.from()+"--"+(-e.weight())+"-->");
				}
				if(sp.distTo(v) != Double.POSITIVE_INFINITY)
					sb.append(v+" "+(-sp.distTo(v))+"\n");
				else
					sb.append("unreacheable!");
				StdOut.println(sb.toString());
			}
		}
		
	}

}

package com.example.algs.graph;

import com.example.algs.In;
import com.example.algs.StdOut;
import com.example.algs.collections.Queue;
import com.example.algs.collections.Stack;

public class BreadFirstPaths {

	private boolean [] marked;
	private int 	[] edgeTo;
	private final int s;
	public BreadFirstPaths(Graph g,int s) {
		// TODO Auto-generated constructor stub
		edgeTo = new int[g.V()];
		marked = new boolean[g.V()];
		this.s = s;
		bfs(g,s);
	}
	private void bfs(Graph g, int s) {
		// TODO Auto-generated method stub
		Queue<Integer> queue = new Queue<Integer>();
		marked[s] = true;
		queue.enqueue(s);
		while(!queue.isEmpty()){
			int v = queue.dequeue();
			for(int w:g.adj(v))
				if(!marked[w])
				{
					edgeTo[w] = v;
					marked[w] = true;
					queue.enqueue(w);
				}
		}
	}
	
	public boolean hasPathTo(int v)
	{
		return marked[v];
	}
	public Iterable<Integer> pathTo(int v)
	{
		if(!hasPathTo(v)) return null;
		Stack<Integer> path = new Stack<Integer>();
		for (int x = v; x != s; x = edgeTo[x])
			path.push(x);
		path.push(s);
		return path;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Graph g = new Graph(new In("tinyCG.txt"));
		Graph g = Graph.fromString(new In("graph116V3350E.txt"));
		BreadFirstPaths paths = new BreadFirstPaths(g,9);
		for(int i = 1; i < g.V(); i++){
			Stack<Integer> s = (Stack<Integer>) paths.pathTo(i);
			StdOut.print(paths.s+" to "+i+":");
			while(!s.isEmpty())
			{
				StdOut.print(s.pop());
				if(!s.isEmpty()){
					StdOut.print("-");
				}
			}
			StdOut.println();
		}
	}
}

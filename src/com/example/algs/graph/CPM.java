package com.example.algs.graph;

import com.example.algs.In;
import com.example.algs.StdIn;
import com.example.algs.StdOut;

public class CPM {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		In in = new In("jobsPC.txt");
		int N = in.readInt();
		in.readLine();
		EdgeWeightedDigraph G;
		G = new EdgeWeightedDigraph(2*N+2);
		int s = 2*N,t = 2*N+1;
		for(int i = 0; i < N; i++){
			String [] a = in.readLine().split("\\s+");
			double duration = Double.parseDouble(a[0]);
			G.addEdge(new DirectedEdge(i,i+N,duration));
			G.addEdge(new DirectedEdge(s,i,0.0));
			G.addEdge(new DirectedEdge(i+N,t,0.0));
			for	(int j = 1; j < a.length; j++)
			{
				int successor = Integer.parseInt(a[j]);
				G.addEdge(new DirectedEdge(i+N,successor,0.0));
			}
		}
		AcycliscLP lp = new AcycliscLP(G,s);
		StdOut.println("Start Times:");
		for	(int i = 0; i < N; i++)
			StdOut.printf("%4d: %5.1f\n",i,lp.distTo(i));
		StdOut.printf("Finish Time:%5.1f\n", lp.distTo(t));
	}

}

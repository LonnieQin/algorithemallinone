package com.example.algs.graph;

import com.example.algs.In;
import com.example.algs.StdOut;

public class Cycle {

	private boolean marked[];
	private boolean hasCycle;
	public Cycle(Graph G) 
	{
		// TODO Auto-generated constructor stub
		marked = new boolean[G.V()];
		for	(int s = 0; s < G.V(); s++)
			if(!marked[s])
				dfs(G,s,s);
	}

	private void dfs(Graph G, int v, int u) {
		// TODO Auto-generated method stub
		marked[v] = true;
		for(int w:G.adj(v))
			if(!marked[w])
				dfs(G,w,v);
			else if (w != u) hasCycle = true;
	}
	
	public boolean hasCycle()
	{
		return hasCycle;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Graph G = Graph.fromString(new In("graph566V83332E.txt"));
		Cycle cycle = new Cycle(G);
		StdOut.println("has cycle:"+cycle.hasCycle());
		G.addEdge(0, 1);
		StdOut.println("has cycle:"+cycle.hasCycle());
	}

}

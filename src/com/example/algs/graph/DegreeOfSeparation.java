package com.example.algs.graph;

import com.example.algs.StdIn;
import com.example.algs.StdOut;

public class DegreeOfSeparation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SymbolGraph sg = new SymbolGraph("routes.txt"," ");
		Graph G = sg.G();
		
		String source = "JFK";
		
		if(!sg.contains(source))
		{	StdOut.println(source+" "+ "not in database."); return;}
		
		int s = sg.index(source);
		BreadFirstPaths bfs = new BreadFirstPaths(G,s);
		
		while(!StdIn.isEmpty())
		{
			String sink = StdIn.readLine();
			if(sg.contains(sink))
			{
				int t = sg.index(sink);
				if(bfs.hasPathTo(t))
					for	(int v : bfs.pathTo(t))
						StdOut.println("   "+sg.name(v));
				else StdOut.println("Not connected");
			}
			else StdOut.println("Not in database.");
		}
	}

}

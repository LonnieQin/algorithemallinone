package com.example.algs.graph;

import com.example.algs.StdOut;
import com.example.algs.collections.Stack;

public class DepthFirstDirectedPaths {

	private boolean [] marked;		//Has dfs() been called for this vertex?
	private int [] edgeTo;			//last vertex on known path to this vertex
	private final int s;			//source
	
	public DepthFirstDirectedPaths(DiGraph G,int s) {
		// TODO Auto-generated constructor stub
		marked = new boolean[G.V()];
		edgeTo = new int[G.V()];
		this.s = s;
		dfs(G,s);
	}

	private void dfs(DiGraph g, int v) {
		// TODO Auto-generated method stub
		marked[v] = true;
		for(int w:g.adj(v))
		{
			if(!marked[w])
			{
				edgeTo[w] = v;
				dfs(g,w);
			}
		}
	}

	public boolean hasPathTo(int v)
	{
		return marked[v];
	}
	
	public Iterable<Integer> pathTo(int v)
	{
		if(!hasPathTo(v))return null;
		Stack<Integer> path = new Stack<Integer>();
		for(int x = v; x != s; x = edgeTo[v]){
			path.push(x);
		}
		path.push(s);
		return path;
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SymbolDiGraph SG = new SymbolDiGraph("foodchain.txt","/");
		DiGraph G = SG.G();
		StdOut.println(SG);
		DepthFirstDirectedPaths path = new DepthFirstDirectedPaths(G,SG.index("grass"));
		for(int v = 0; v < G.V(); v++){
			String str = "";
			boolean firstTime = true;
			for(int w:path.pathTo(v)){
				if(firstTime){
					firstTime = false;
					str += "("+w+")"+SG.name(w);
				}else{
					str += "=>("+w+")"+SG.name(w);
				}
			}
			StdOut.println(v+":"+str);
		}
	}

}

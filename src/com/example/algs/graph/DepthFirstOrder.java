package com.example.algs.graph;

import com.example.algs.StdOut;
import com.example.algs.collections.Queue;
import com.example.algs.collections.Stack;

public class DepthFirstOrder {

	private boolean [] marked;
	private Queue<Integer> pre;
	private Queue<Integer> post;
	private Queue<Integer> preorder;
	private Queue<Integer> postorder;
	private Stack<Integer> reversePost;
	private int preCounter;		//counter or preorder numbering
	private int postCounter;	//counter for postorder numbering
	
	public DepthFirstOrder(DiGraph G) {
		// TODO Auto-generated constructor stub
		pre = new Queue<Integer>();
		post = new Queue<Integer>();
		reversePost = new Stack<Integer>();
		marked = new boolean[G.V()];
		
		for	(int v = 0; v < G.V(); v++)
			if	(!marked[v]) dfs(G,v);
	}
	
	public DepthFirstOrder(EdgeWeightedDigraph G)
	{
		pre = new Queue<Integer>();
		post = new Queue<Integer>();
		reversePost = new Stack<Integer>();
		postorder = new Queue<Integer>();
		preorder = new Queue<Integer>();
		marked = new boolean[G.V()];
		for	(int v = 0; v < G.V(); v++)
			if(!marked[v])dfs(G,v);
	}

	private void dfs(DiGraph g, int v) {
		// TODO Auto-generated method stub
		pre.enqueue(v);
		marked[v] = true;
		for(int w:g.adj(v))
			if(!marked[w])
				dfs(g,w);
		post.enqueue(v);
		reversePost.push(v);
	}
	private void dfs(EdgeWeightedDigraph G,int v)
	{
		marked[v] = true;
		pre.enqueue(v);
		for(DirectedEdge e:G.adj(v)){
			int w = e.to();
			if(!marked[w]){
				dfs(G,w);
			}
		}
		post.enqueue(v);
		reversePost.push(v);
	}
	
	public Iterable<Integer> pre()
	{	return pre;				}
	public Iterable<Integer> post()
	{	return post;			}
	public Iterable<Integer> reversePost()
	{	return reversePost;		}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SymbolDiGraph SG = new SymbolDiGraph("digraph3.txt","/");
		DepthFirstOrder order = new DepthFirstOrder(SG.G());
		String str = "";
		boolean firstTime = true;
		for(int i:order.pre()){
			if(firstTime){
				str += "("+i+")"+SG.name(i);
				firstTime = false;
			}else{
				str += "->("+i+")"+SG.name(i);
			}
		}
		StdOut.println("preorder:"+str);
		firstTime = true;
		str="";
		for(int i:order.post()){
			if(firstTime){
				str += "("+i+")"+SG.name(i);
				firstTime = false;
			}else{
				str += "->("+i+")"+SG.name(i);
			}
		}
		StdOut.println("postorder:"+str);
		firstTime = true;
		str="";
		for(int i:order.reversePost()){
			if(firstTime){
				str += "("+i+")"+SG.name(i);
				firstTime = false;
			}else{
				str += "->("+i+")"+SG.name(i);
			}
		}
		StdOut.println("revertpostorder:"+str);	
		
	}

}

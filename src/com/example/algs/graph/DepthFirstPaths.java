package com.example.algs.graph;

import com.example.algs.In;
import com.example.algs.StdOut;
import com.example.algs.collections.Stack;

public class DepthFirstPaths {

	private boolean [] marked;
	private int [] edgeTo;
	private final int s;
	public DepthFirstPaths(Graph G, int s) {
		// TODO Auto-generated constructor stub
		marked = new boolean[G.V()];
		edgeTo = new int[G.V()];
		this.s = s;
		dfs(G,s);
	}
	
	private void dfs(Graph G, int v)
	{
		marked[v] = true;
		for(int w:G.adj(v)){
			if (!marked[w]){
				edgeTo[w] = v;
				dfs(G,w);
			}
		}
	}
	
	public boolean hasPathTo(int v)
	{
		return marked[v];
	}
	
	public Iterable<Integer> pathTo(int v)
	{
		if(!hasPathTo(v))	return null;
		Stack<Integer> path = new Stack<Integer>();
		for	(int x = v; x != s; x = edgeTo[x])
			path.push(x);
		path.push(s);
		return path;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Graph g = new Graph(new In("tinyCG.txt"));
		Graph g = Graph.fromString(new In("graph566V83332E.txt"));
		//StdOut.println(g);
		DepthFirstPaths paths = new DepthFirstPaths(g,0);
		Stack<Integer> s = (Stack<Integer>) paths.pathTo(24);
		while(!s.isEmpty())
		{
			StdOut.print(s.pop()+" ");
		}
		StdOut.print();
	}

}

package com.example.algs.graph;

import com.example.algs.In;
import com.example.algs.StdOut;

public class DepthFirstSearch {

	public boolean [] marked;
	public int count;
	public DepthFirstSearch(Graph G,int s) {
		// TODO Auto-generated constructor stub
		marked = new boolean[G.V()];
		dfs(G,s);
	}
	
	private void dfs(Graph G,int v)
	{
		StdOut.println("dfs(G,"+v+")");
		marked[v] = true;
		count ++;
		for(int w:G.adj(v))
			if(!marked[w])dfs(G,w);
	}
	
	public boolean marked(int w)
	{	
		return marked[w];		
	}
	
	public int count()
	{
		return count;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Graph g = Graph.fromString(new In("graph116V3350E.txt"));
		Graph g = new Graph(new In("tinyCG.txt"));
		StdOut.println(g);
		DepthFirstSearch dfs = new DepthFirstSearch(g,0);
	}

}

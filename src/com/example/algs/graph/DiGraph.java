package com.example.algs.graph;

import com.example.algs.In;
import com.example.algs.Out;
import com.example.algs.StdOut;
import com.example.algs.StdRandom;
import com.example.algs.StopWatch;
import com.example.algs.collections.Bag;

public class DiGraph {
	final int V;
	int E;
	Bag<Integer> [] adj;
	int [] outDegree;
	int [] inDegree;
	public DiGraph(In in) {
		// TODO Auto-generated constructor stub
		this(in.readInt());
		int E = in.readInt();
		for(int i = 0; i < E; i++)
		{
			int v = in.readInt();
			int w = in.readInt();
			addEdge(v,w);
		}
	}
	
	public DiGraph(int V)
	{
		this.V = V;
		this.E = 0;
		adj = (Bag<Integer>[]) new Bag[V];
		outDegree = new int[V];
		inDegree = new int[V];
		for(int i = 0; i < V; i++)
		{
			adj[i] = new Bag<Integer>();
			outDegree[i] = 0;
			inDegree[i] = 0;
		}
		
	}
	
	public void addEdge(int v,int w)
	{
		adj[v].add(w);
		outDegree[v]++;
		inDegree[w]++;
		E++;
	}
	
	public int outDegree(int v)
	{
		return outDegree[v];
	}
	
	public int inDegree(int v)
	{
		return inDegree[v];
	}

	public static DiGraph instance(int length){
		int [] a = new int[length];
		for(int i = 0; i < a.length;i++){
			a[i] = i;
		}
		DiGraph g = new DiGraph(length);
		for(int v = 0; v < g.V(); v++){
			StdRandom.shuffle(a);
			int numberOfv = StdRandom.uniform(0,g.V());
			for(int j = 0; j < numberOfv;j++)
			{
				int w = a[j];
				if(v != w)
				   g.addEdge(v, w);
			}
		}
		return g;
	}
	
	public int V()
	{
		return V;
	}
	
	public int E()
	{
		return E;
	}
	
	
	Iterable<Integer> adj(int v)
	{
		return adj[v];
	}
	
	public DiGraph reverse()
	{
		DiGraph G = new DiGraph(V);
		for(int v = 0; v < V; v++)
			for(int w:adj[v])
				G.addEdge(w, v);
		return G;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(V+" vertices,"+E+" edges\n");
		for(int i = 0; i < V; i++){
			sb.append(i+":");
			for(int j:adj[i]){
				sb.append(" "+j);
			}
			sb.append("\n");
		}
		return sb.toString();
	}
	public static DiGraph fromString(In in)
	{
		String s = in.readLine();
		String [] ss = s.split("\\s+");
		DiGraph g = new DiGraph(Integer.parseInt(ss[0]));
		while(!in.isEmpty())
		{
			s = in.readLine();
			ss = s.split("\\s+");
			int v = Integer.parseInt(ss[0].split(":")[0]);
			for(int i = 1; i < ss.length; i++){
				int w = Integer.parseInt(ss[i]);
				g.addEdge(v, Integer.parseInt(ss[i]));
			}
		}
		return g;
	}
	
	public void saveToFile()
	{
		saveToFile("graph"+this.V()+"V"+this.E()+"E.txt");
	}
	public void saveToFile(String str)
	{
		Out out = new Out(str);
		out.println(this.toString());
		out.close();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DiGraph G = DiGraph.instance(100);
		G.saveToFile("digraph2.txt");
		G = DiGraph.fromString(new In("digraph2.txt"));
		StdOut.println(G);
		for(int i = 0; i < G.V(); i++){
			StdOut.println(i+" indegree:"+G.inDegree(i)+" outdegree:"+G.outDegree(i));
		}
	}

}

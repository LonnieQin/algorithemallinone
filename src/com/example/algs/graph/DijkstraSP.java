package com.example.algs.graph;

import com.example.algs.In;
import com.example.algs.StdOut;
import com.example.algs.collections.IndexMinPQ;
import com.example.algs.collections.Stack;

public class DijkstraSP {

	private DirectedEdge[] edgeTo;
	private double [] distTo;
	private IndexMinPQ<Double> pq;
	public DijkstraSP(EdgeWeightedDigraph G, int s) {
		// TODO Auto-generated constructor stub
		edgeTo = new DirectedEdge[G.V()];
		distTo = new double[G.V()];
		pq = new IndexMinPQ<Double>(G.V());
		for(int v = 0; v < G.V(); v++)
			distTo[v] = Double.POSITIVE_INFINITY;
		distTo[s] = 0.0;
		
		pq.insert(s, 0.0);
		while(!pq.isEmpty())
		{
			int v = pq.delMin();
			for	(DirectedEdge e: G.adj(v))
				relax(e);
		}
	}

	public double distTo(int v)
	{
		return distTo[v];
	}
	public boolean hasPathTo(int v)
	{
		return distTo[v] < Double.POSITIVE_INFINITY;
	}
	
	public Iterable<DirectedEdge> pathTo(int v)
	{
		Stack<DirectedEdge> path = new Stack<DirectedEdge>();
		for(DirectedEdge e = edgeTo[v]; e!= null; e = edgeTo[e.from()])
			path.push(e);
		return path;
	}
	
	private void relax(DirectedEdge e) {
		// TODO Auto-generated method stub
		int v = e.from(),w = e.to();
		double dist = distTo[v]+e.weight;
		if(distTo[w] > dist)
		{
			distTo[w] = dist;
			edgeTo[w] = e;
			if(pq.contains(w))	pq.decreaseKey(w, distTo[w]);
			else				pq.insert(w, distTo[w]);
			
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EdgeWeightedDigraph EWDG = new EdgeWeightedDigraph(new In("dijkstrasp.txt"));
		for(int w = 0; w < EWDG.V();w++){
			DijkstraSP sp = new DijkstraSP(EWDG,w);
			for(int v = 0; v < EWDG.V();v++){
				if(v == w) continue;
				StringBuilder sb = new StringBuilder();
				sb.append(w+" to "+v+":");
				for(DirectedEdge e:sp.pathTo(v)){
					sb.append(e.from()+"--"+e.weight()+"-->");
				}
				if(sp.distTo(v) != Double.POSITIVE_INFINITY)
					sb.append(v+" "+sp.distTo(v)+"\n");
				else
					sb.append("unreacheable!");
				StdOut.println(sb.toString());
			}
		}
	}

}

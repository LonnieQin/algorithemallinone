package com.example.algs.graph;

import com.example.algs.StdOut;
import com.example.algs.collections.Queue;

public class DirectedBFS {
	public boolean [] marked;
	Queue<Integer> q;
	Queue<Integer> backup;
	public DirectedBFS(DiGraph G,int s) {
		// TODO Auto-generated constructor stub
		marked = new boolean[G.V()];
		q = new Queue<Integer>();
		backup = new Queue<Integer>();
		bfs(G,s);
	}
	private void bfs(DiGraph g, int s) {
		// TODO Auto-generated method stub
		q.enqueue(s);
		marked[s] = true;
		while(!q.isEmpty()){
			int v = q.dequeue();
			backup.enqueue(v);
			for	(int w : g.adj(v))
				if(!marked[w])
				{
					marked[w] = true;
					q.enqueue(w);
				}
		}
		//for()
	}
	public DirectedBFS(DiGraph G,Iterable<Integer> sources)
	{
		marked = new boolean[G.V()];
		q = new Queue<Integer>();
		backup = new Queue<Integer>();
		bfs(G,sources);
	}

	private void bfs(DiGraph g, Iterable<Integer> sources) {
		// TODO Auto-generated method stub
		for(int v:sources){
			q.enqueue(v);
			marked[v] = true;
		}
		while(!q.isEmpty())
		{
			int v = q.dequeue();
			backup.enqueue(v);
			for(int w:g.adj(v)){
					if(!marked[w]){
						marked[w] = true;
						q.enqueue(w);
					}
			}
		}
	}
	
	public Iterable<Integer> queue(){
		return backup;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SymbolDiGraph SG = new SymbolDiGraph("digraph3.txt","/");
		Queue<Integer> vetexs = new Queue<Integer>();
		vetexs.enqueue(SG.index("Calculus"));
		vetexs.enqueue(SG.index("Algorithms"));
		//DirectedBFS bfs = new DirectedBFS(SG.G(),SG.index("Calculus"));
		DirectedBFS bfs = new DirectedBFS(SG.G(),vetexs);
		String str = "";
		boolean firsttime = true;
		for(int w:bfs.queue()){
			if(!firsttime)
				str += "=>("+w+")"+SG.name(w);
			else{
				firsttime=false;
				str += "("+w+")"+SG.name(w);
			}
		}
		StdOut.println(str);
		
	}

}
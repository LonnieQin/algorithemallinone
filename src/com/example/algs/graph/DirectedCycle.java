package com.example.algs.graph;

import com.example.algs.In;
import com.example.algs.StdOut;
import com.example.algs.collections.Stack;

public class DirectedCycle {

	public boolean [] marked;
	private int [] edgeTo;
	private Stack<Integer> cycle;	//vertices on a cycle (if one exists)
	private boolean[] onStack;		//vertices on recursive call stack
	public DirectedCycle(DiGraph G) 
	{
		// TODO Auto-generated constructor stub
		onStack = new boolean[G.V()];
		edgeTo = new int[G.V()];
		marked = new boolean[G.V()];
		for	(int v = 0; v < G.V(); v++)
			if	(!marked[v]) dfs(G,v);
	}
	
	private void dfs(DiGraph g, int v) {
		// TODO Auto-generated method stub
		onStack[v] = true;
		marked[v] = true;
		for	(int w:g.adj(v))
			if(this.hasCycle())	return;
			else if (!marked[w])
			{	edgeTo[w] = v; dfs(g,w);}
			else if (onStack[w])
			{
				cycle = new Stack<Integer>();
				for	(int x = v; x != w; x = edgeTo[x])
					cycle.push(x);
				cycle.push(w);
				cycle.push(v);
			}
		onStack[v] = false;
	}

	public boolean hasCycle()
	{
		return cycle != null;
	}
	
	Iterable<Integer> cycle()
	{
		return cycle;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DiGraph G = DiGraph.fromString(new In("digraph2.txt"));
		DirectedCycle cycle = new DirectedCycle(G);
		StdOut.println(cycle.hasCycle());
		
	}

}

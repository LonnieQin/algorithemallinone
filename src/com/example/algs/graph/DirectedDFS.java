package com.example.algs.graph;

import com.example.algs.In;
import com.example.algs.StdOut;
import com.example.algs.collections.Bag;

public class DirectedDFS {

	public boolean marked[];
	public DirectedDFS(DiGraph G,int s)
	{
		marked = new boolean[G.V()];
		dfs(G,s);
	}
	private void dfs(DiGraph G,int v)
	{
		marked[v] = true;
		for(int w:G.adj(v))
		{
			if(!marked[w])
				dfs(G,w);
		}
	}
	public DirectedDFS(DiGraph G,Iterable<Integer> sources)
	{
		marked = new boolean[G.V()];
		for(int s:sources)
		{
			if(!marked[s]){
				StdOut.println("s:"+s);
				dfs(G,s);
			}
		}
	}
	public boolean marked(int v)
	{
		return marked[v];
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DiGraph G = DiGraph.fromString(new In("digraph2.txt"));
		StdOut.println(G);
		Bag<Integer> b = new Bag<Integer>();
		b.add(22);
		b.add(33);
		b.add(4);
		DirectedDFS reachable = new DirectedDFS(G,b);
		for	(int v = 0; v < G.V(); v++)
			if(reachable.marked(v))StdOut.println(v+" ");
		StdOut.println();
	}

}

package com.example.algs.graph;

public class DirectedEdge {

	public final int v,w;
	public final double weight;
	public DirectedEdge(int v,int w,double weight) {
		// TODO Auto-generated constructor stub
		this.v = v;
		this.w = w;
		this.weight = weight;
	}
	
	public int from()
	{
		return v;
	}
	
	public int to()
	{
		return w;
	}
	
	public double weight()
	{
		return weight;
	}
	
	public String toString()
	{
		return v+"--"+weight+"-->"+w;
	}
}

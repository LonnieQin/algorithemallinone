package com.example.algs.graph;

public class Edge implements Comparable<Edge>{

	private final double weight;
	private final int v;
	private final int w;
	public Edge(int v, int w, double weight) {
		// TODO Auto-generated constructor stub
		this.weight = weight;
		this.v = v;
		this.w = w;
	}
	
	public int either()						//either endpoint
	{
		return v;
	}
	
	public int orther(int vertex)				//the end point that's not v
	{
		if(vertex == v)	return w;
		return v;
	}
	
	public int compareTo(Edge that)			//compare this edge to that edge
	{
		if	(this.weight < that.weight)		return -1;
		else if (this.weight > that.weight) return +1;
		else								return 0;
	}
	
	public double weight()					//the weight
	{
		return weight;
	}
	
	public String toString()				//string representation
	{
		return v+" "+w+" "+weight;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

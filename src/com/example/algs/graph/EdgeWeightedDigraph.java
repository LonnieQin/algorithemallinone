package com.example.algs.graph;

import com.example.algs.In;
import com.example.algs.StdOut;
import com.example.algs.collections.Bag;
import com.example.algs.collections.Queue;

public class EdgeWeightedDigraph {
	private final int V;
	private int E;
	private Bag<DirectedEdge> adj[];
	public EdgeWeightedDigraph(int V) {
		// TODO Auto-generated constructor stub
		this.V = V;
		E = 0;
		adj = (Bag<DirectedEdge>[])new Bag[V];
		for(int v = 0; v < V; v++)
			adj[v] = new Bag<DirectedEdge>();
	}
	
	public EdgeWeightedDigraph(In in){
		this(in.readInt());
		int E = in.readInt();
		for(int i = 0; i < E; i++){
			int v = in.readInt();
			int w = in.readInt();
			double weight = in.readDouble();
			DirectedEdge e = new DirectedEdge(v,w,weight);
			addEdge(e);
		}
	}
	
	public void addEdge(DirectedEdge e)
	{
		int v = e.from();
		adj[v].add(e);
		E++;
	}
	
	public Iterable<DirectedEdge> adj(int v)
	{
		return adj[v];
	}
	
	public int V()
	{
		return V;
	}
	
	public int E()
	{
		return E;
	}
	
	public Iterable<DirectedEdge> edges()
	{
		Bag<DirectedEdge> q = new Bag<DirectedEdge>();
		for(int v = 0; v < V; v++)
			for(DirectedEdge e:adj[v])
				q.add(e);
		return q;
	}
	
	public String toString()
	{
		String str = "";
		for(int v = 0; v < V; v++){
			str += v+":";
			for(DirectedEdge e:adj[v]){
				str += e.toString()+" ";
			}
			str +="\n";
		}
		return str;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EdgeWeightedDigraph ewd = new EdgeWeightedDigraph(new In("edgeweighteddigraph.txt"));
		StdOut.println(ewd);
	}

}

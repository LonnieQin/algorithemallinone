package com.example.algs.graph;

import com.example.algs.collections.Stack;

public class EdgeWeightedDirectedCycle {
	public boolean [] marked;
	private DirectedEdge [] edgeTo;
	private Stack<DirectedEdge> cycle;	//vertices on a cycle (if one exists)
	private boolean[] onStack;		//vertices on recursive call stack
	public EdgeWeightedDirectedCycle() {
		// TODO Auto-generated constructor stub
	}

	public EdgeWeightedDirectedCycle(EdgeWeightedDigraph G) {
		// TODO Auto-generated constructor stub
		onStack = new boolean[G.V()];
		edgeTo = new DirectedEdge[G.V()];
		marked = new boolean[G.V()];
		for	(int v = 0; v < G.V(); v++)
			if	(!marked[v]) dfs(G,v);
	}
	private void dfs(EdgeWeightedDigraph g, int v) {
		// TODO Auto-generated method stub
		onStack[v] = true;
		marked[v] = true;
		for	(DirectedEdge e:g.adj(v)){
			int w = e.to();
			if(hasCycle())	return;
			else if (!marked[w])
			{	edgeTo[w] = e; dfs(g,w);}
			else if (onStack[w])
			{
				cycle = new Stack<DirectedEdge>();
				while(e.from() != w){
					cycle.push(e);
					e = edgeTo[e.from()];
				}
				cycle.push(e);
			}
		}
		onStack[v] = false;
	}

	public boolean hasCycle()
	{
		return cycle != null;
	}
	
	Iterable<DirectedEdge> cycle()
	{
		return cycle;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

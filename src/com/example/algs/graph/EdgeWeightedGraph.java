package com.example.algs.graph;
import com.example.algs.*;
import com.example.algs.collections.Bag;
import com.example.algs.collections.Queue;
public class EdgeWeightedGraph {

	private final int V;
	private int E;
	private final Bag<Edge>[] adj;
	private Queue<Edge> edges;
	/**
	 * create an graph with random vertices and random number random weighted edges
	 */
	public EdgeWeightedGraph()
	{
		this(StdRandom.uniform(10,100));
		for(int v = 0; v < V; v++){
			for(int w = v+1; w<V; w+=StdRandom.uniform(1,5)){
				double d = StdRandom.uniform();
				Edge e = new Edge(v,w,d);
				addEdge(e);
			}
		}
	}
	/**
	 * create an empty graph with V vertices
	 * @param V
	 */
	public EdgeWeightedGraph(int V) 
	{
		// TODO Auto-generated constructor stub
		this.V = V;
		E = 0;
		adj = (Bag<Edge>[]) new Bag[V];
		edges = new Queue<Edge>();
		for	(int v = 0; v < V; v++)
			adj[v] = new Bag<Edge>();
	}
	
	/**
	 * create a graph from input stream
	 * @param in
	 */
	public EdgeWeightedGraph(In in)
	{
		this(in.readInt());
		int E = in.readInt();
		while(!in.isEmpty()){
			int v = Integer.parseInt(in.readString());
			int w = Integer.parseInt(in.readString());
			double d = in.readDouble();
			Edge e = new Edge(v,w,d);
			addEdge(e);
		}
	}
	
	/**
	 * add weighted edge e to this graph
	 * @param e
	 */
	public void addEdge(Edge e)
	{
		int v = e.either(),w = e.orther(v);
		adj[v].add(e);
		edges.enqueue(e);
		adj[w].add(e);
		E++;
	}
	
	/**
	 * edges incident to v
	 * @param v
	 * @return
	 */
	Iterable<Edge> adj(int v)
	{
		return adj[v];
	}
	
	
	/**
	 * number of vertexs
	 * @return
	 */
	public int V()
	{
		return V;
	}
	
	/**
	 * number of edges
	 * @return
	 */
	public int E()
	{
		return E;
	}

	/**
	 * string representation
	 */
	public String toString()
	{
		String str = "";
		str += V+"\n";
		str += E+"\n";
		for(int v = 0; v < V;v++){
			str += v+":";
			for(Edge e:adj[v]){
				str+="=>"+e.either()+" "+e.orther(e.either())+" "+e.weight();
			}
			str+="\n";
		}
		return str;
	}
	public void saveToFile(String fileName)
	{
		Out out = new Out(fileName);
		out.println(V);
		out.println(E);
		for(int v = 0; v < V;v++){
			for(Edge e:adj[v]){
				int v1 = e.either();
				int v2 = e.orther(v1);
				double d = e.weight();
				if(v1 == v)
					out.println(v1+" "+v2+" "+d);
			}
		}
		out.close();
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EdgeWeightedGraph g = new EdgeWeightedGraph(new In("tinyEWG.txt"));
		//EdgeWeightedGraph g = new EdgeWeightedGraph();
		StdOut.println(g);
		EdgeWeightedGraph randomGraph = new EdgeWeightedGraph();
		randomGraph.saveToFile("weightedgraph.txt");
		EdgeWeightedGraph copy = new EdgeWeightedGraph(new In("weightedgraph.txt"));
		StdOut.println(copy);
	}
	public Iterable<Edge> edges() {
		// TODO Auto-generated method stub
		return edges;
	}

}

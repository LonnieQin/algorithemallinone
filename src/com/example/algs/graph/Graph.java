package com.example.algs.graph;

import com.example.algs.In;
import com.example.algs.Out;
import com.example.algs.StdOut;
import com.example.algs.StdRandom;
import com.example.algs.StopWatch;
import com.example.algs.collections.Bag;

public class Graph {
	private final int V;		//number of vertices
	private int E;				//number of edges
	private Bag<Integer>[] adj;	//adjacency lists
	
	public Graph(int V) {
		// TODO Auto-generated constructor stub
		this.V = V;
		this.E = 0;
		adj = (Bag<Integer>[]) new Bag[V];
		for	(int v = 0; v < V; v++)
			adj[v] = new Bag<Integer>();
	}
	
	public Graph(In in){
		this(in.readInt());
		int E = in.readInt();
		for(int i = 0; i < E; i++)
		{
			int v = in.readInt();
			int w = in.readInt();
			addEdge(v,w);
		}
	}
	
	public static Graph instance(int length){
		
		Graph g = new Graph(length);
		for(int v = 0; v < g.V()-1; v++){
			StopWatch timer = new StopWatch();
			int numberOfv = StdRandom.uniform(g.V());
			int [] a = new int[g.V()-v-1];
			for(int i = 0; i < a.length;i++){
				a[i] = i+v+1;
			}
			for(int j = g.degree(v); j < numberOfv; j++){
				while(true)
				{
					int num = StdRandom.uniform(0,a.length);
					for(int count=0; count < a.length && a[num] == -1;num=(num+1)%a.length,count++);
					if(a[num] != -1)
					{
						g.addEdge(v, a[num]);
						a[num] = -1;
					}
					break;
				}
			}
			StdOut.println(v+" "+timer.elapsedTime());
		}
		return g;
	}
	
	public int V(){
		return V;
	}
	
	public int E(){
		return E;
	}
	
	public void addEdge(int v,int w){
		adj[v].add(w);
		adj[w].add(v);
		E++;
	}
	
	public Iterable<Integer> adj(int v){
		return adj[v];
	}
	
	public static int degree(Graph G,int v)
	{
		int degree = 0;
		for(int w:G.adj(v)) degree++;
		return degree;
	}
	
	public static int maxDegree(Graph G)
	{
		int max = 0;
		for(int v = 0; v < G.V();v++)
			if(degree(G,v) > max)
				max = degree(G,v);
		return max;
	}
	
	public static int avgDegree(Graph G)
	{
		return 2 * G.E() / G.V();
	}
	
	public static int numberOfSelfLoops(Graph G)
	{
		int count = 0;
		for(int v = 0; v < G.V(); v++)
			for(int w:G.adj(v))
				if(v == w) count++;
		return count/2;
	}
	
	public int degree(int v)
	{
		if(v > V || v < 0)
			return 0;
		return degree(this,v);
	}
	
	public int maxDegree()
	{
		return maxDegree(this);
	}
	
	public int avgDegree()
	{
		return avgDegree(this);
	}
	
	public int numberOfSelfLoops()
	{
		return numberOfSelfLoops(this);
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(V+" vertices,"+E+" edges\n");
		for(int i = 0; i < V; i++){
			sb.append(i+":");
			for(int j:adj[i]){
				sb.append(" "+j);
			}
			sb.append("\n");
		}
		return sb.toString();
	}
	public static Graph fromString(In in)
	{
		String s = in.readLine();
		String [] ss = s.split("\\s+");
		Graph g = new Graph(Integer.parseInt(ss[0]));
		while(!in.isEmpty())
		{
			s = in.readLine();
			ss = s.split("\\s+");
			int v = Integer.parseInt(ss[0].split(":")[0]);
			for(int i = 1; i < ss.length; i++){
				int w = Integer.parseInt(ss[i]);
				if(w >= v)
					g.addEdge(v, Integer.parseInt(ss[i]));
			}
		}
		return g;
	}
	
	public void saveToFile()
	{
		saveToFile("graph"+this.V()+"V"+this.E()+"E.txt");
	}
	public void saveToFile(String str)
	{
		Out out = new Out(str);
		out.println(this.toString());
		out.close();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Graph graph = Graph.fromString(new In("graph83.txt"));
		StdOut.println(graph);
		StdOut.println("Max degree:"+graph.maxDegree());
		StdOut.println("Average degree:"+graph.avgDegree());
		graph.addEdge(1, 1);
		graph.addEdge(9, 9);
		StdOut.println("Number of self loops:"+graph.numberOfSelfLoops());
		//Graph graph2 = Graph.instance();
		//graph2.saveToFile();
		Graph graph3 = Graph.fromString(new In("graph566V83332E.txt"));
		StdOut.println(graph3);
		StdOut.println("Max degree:"+graph3.maxDegree());
		StdOut.println("Average degree:"+graph3.avgDegree());
	}

}

package com.example.algs.graph;

import com.example.algs.StdOut;
import com.example.algs.collections.Bag;

public class KosarajuSCC {

	private boolean [] marked;	//reached vertices
	private int[] id;			//component identifiers
	private int count;			//number of strong components
	public KosarajuSCC(DiGraph G) {
		// TODO Auto-generated constructor stub
		marked = new boolean[G.V()];
		id = new int[G.V()];
		DepthFirstOrder order = new DepthFirstOrder(G.reverse());
		for	(int s:order.reversePost())
			if	(!marked[s])
			{	dfs(G,s); count++;}
	}

	private void dfs(DiGraph g, int v) {
		// TODO Auto-generated method stub
		marked[v] = true;
		id[v] = count;
		for	(int w:g.adj(v))
			if(!marked[w])
				dfs(g,w);
	}
	
	public boolean stronglyConnected(int v,int w)
	{	return id[v] == id[w];	}

	public int id(int v)
	{	return id[v];}
	
	public int count()
	{	return count;}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SymbolDiGraph DG = new SymbolDiGraph("foodchain.txt","/");
		DiGraph G = DG.G();
		StdOut.println(DG);
		KosarajuSCC cc = new KosarajuSCC(G);
		StdOut.println("Component numbers:"+cc.count());
		Bag<Integer>[] components = (Bag<Integer>[]) new Bag[cc.count()];
		for(int i = 0; i < cc.count(); i++){
			components[i] = new Bag<Integer>();
		}
		for(int v = 0; v < G.V(); v++)
			components[cc.id(v)].add(v);
		for	(int i = 0; i < cc.count(); i++)
		{
			for(int v:components[i])
				StdOut.print("("+v+")"+DG.name(v)+" ");
			StdOut.println();
		}
	}

}

package com.example.algs.graph;

import com.example.I.UF;
import com.example.algs.In;
import com.example.algs.StdOut;
import com.example.algs.collections.MinPQ;
import com.example.algs.collections.Queue;

public class KruskalMST {

	private double weight = 0;
	Queue<Edge> mst = new Queue<Edge>();
	public KruskalMST(EdgeWeightedGraph G) {
		// TODO Auto-generated constructor stub
	    MinPQ<Edge> pq = new MinPQ<Edge>();
		for(Edge e:G.edges())
			pq.insert(e);
		UF uf = new UF(G.V());
		while (!pq.isEmpty() && mst.size() < G.V()-1)
		{
			Edge e = pq.delMin();
			int v = e.either(),w = e.orther(v);
			if(!uf.connected(v, w))
			{
				uf.union(v, w);
				weight += e.weight();
				mst.enqueue(e);
			}
		}
	}
	
	public Iterable<Edge> edges()
	{
		return mst;
	}
	
	public double weight()
	{
		return weight;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EdgeWeightedGraph G = new EdgeWeightedGraph(new In("tinyEWG.txt"));
		KruskalMST mst = new KruskalMST(G);
		for	(Edge e:mst.edges())
			StdOut.println(e);
		StdOut.println("Weight:"+mst.weight());
	}

}

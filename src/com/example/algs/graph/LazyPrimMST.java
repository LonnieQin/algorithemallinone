package com.example.algs.graph;

import com.example.algs.In;
import com.example.algs.StdOut;
import com.example.algs.collections.MinPQ;
import com.example.algs.collections.Queue;

public class LazyPrimMST {

	private boolean [] marked;
	private Queue<Edge> mst;
	private MinPQ<Edge> pq;
	private double weight = 0;
	public LazyPrimMST(EdgeWeightedGraph G) {
		// TODO Auto-generated constructor stub
		pq = new MinPQ<Edge>();
		mst = new Queue<Edge>();
		marked = new boolean[G.V()];
		visit(G,0);
		
		while(!pq.isEmpty() && mst.size() < G.V()-1)
		{
			Edge e = pq.delMin();
			int v = e.either(),w = e.orther(v);
			if(marked[v] && marked[w])continue;
			mst.enqueue(e);
			weight += e.weight();
			if(!marked[v]) visit(G,v);
			if(!marked[w]) visit(G,w);
		}
		
	}

	private void visit(EdgeWeightedGraph G, int v) {
		// TODO Auto-generated method stub
		marked[v] = true;
		for	(Edge e:G.adj(v))
			if(!marked[e.orther(v)]){
				pq.insert(e);
			}
	}

	public Iterable<Edge> mst()
	{
		return mst;
	}
	
	public double weight()
	{
		return weight;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EdgeWeightedGraph G = new EdgeWeightedGraph(new In("tinyEWG.txt"));
		LazyPrimMST mst = new LazyPrimMST(G);
		for(Edge e:mst.mst())
			StdOut.println(e);
		StdOut.println("Weight"+mst.weight());
	}

}
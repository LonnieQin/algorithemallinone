package com.example.algs.graph;

import com.example.algs.In;
import com.example.algs.StdOut;
import com.example.algs.collections.Queue;

public class MST {
	Queue<Edge> edges;
	public MST(EdgeWeightedGraph G) {
		// TODO Auto-generated constructor stub
		edges = new Queue<Edge>();
		
	}
	
	Iterable<Edge> edges(){
		return edges;
	}
	
	public double weight(){
		return 0.0f;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EdgeWeightedGraph G = new EdgeWeightedGraph(new In("tinyEWG"));
		MST mst = new MST(G);
		for	(Edge e:mst.edges())
			StdOut.println(e);
		//StdOut.print("%.2f",mst.weight());
	}

}

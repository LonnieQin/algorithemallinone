package com.example.algs.graph;

import com.example.algs.In;
import com.example.algs.StdOut;
import com.example.algs.collections.IndexMinPQ;
import com.example.algs.collections.Queue;

public class PrimMST {

	private Edge [] edgeTo;			//shortest edge from tree vertex
	private double [] distTo;		//distTo[w] = edgeTo[w].weight()
	private boolean [] marked;		//true if v on tree
	private IndexMinPQ<Double> pq;	//eligiblecrossing edges
	public PrimMST(EdgeWeightedGraph G) {
		// TODO Auto-generated constructor stub
		edgeTo = new Edge[G.V()];
		distTo = new double[G.V()];
		marked = new boolean[G.V()];
		for	(int v = 0; v < G.V(); v++)
			distTo[v] = Double.POSITIVE_INFINITY;
		pq = new IndexMinPQ<Double>(G.V());
		distTo[0] = 0.0;
		pq.insert(0, 0.0);
		while(!pq.isEmpty())
			visit(G,pq.delMin());
	}

	private void visit(EdgeWeightedGraph G, int v) {
		// TODO Auto-generated method stub
		marked[v] = true;
		for	(Edge e:G.adj(v))
		{
			int w = e.orther(v);
			if	(marked[w]) continue;
			if	(e.weight() < distTo[w])
			{
				edgeTo[w] = e;
				distTo[w] = e.weight();
				if	(pq.contains(w)) pq.change(w,distTo[w]);
				else				 pq.insert(w, distTo[w]);
			}
		}
	}

	public Iterable<Edge> edges()
	{
		Queue<Edge> mst = new Queue<Edge>();
		for	(int v = 0; v < edgeTo.length; v++){
			Edge e = edgeTo[v];
			if(e != null){
				mst.enqueue(e);
			}
		}
		return mst;
	}
	
	public double weight()
	{
		double weight = 0;
		for	(Edge e:edges())
			weight += e.weight();
		return weight;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EdgeWeightedGraph G = new EdgeWeightedGraph(new In("tinyEWG.txt"));
		PrimMST mst = new PrimMST(G);
		for	(Edge e:mst.edges())
			StdOut.println(e);
		StdOut.println("Weight:"+mst.weight());
	}

}

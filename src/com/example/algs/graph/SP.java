package com.example.algs.graph;

import com.example.algs.collections.Stack;

public class SP {

	private final int s;
	public DirectedEdge [] edgeTo;
	public double distTo[];
	public SP(EdgeWeightedDigraph G,int s) {
		// TODO Auto-generated constructor stub
		this.s = s;
		distTo = new double[G.V()];
		edgeTo = new DirectedEdge[G.V()];
	}
	public double distTo(int v)
	{
		return distTo[v];
	}
	public boolean hasPathTo(int v)
	{
		return false;
	}
	
	public Iterable<DirectedEdge> pathTo(int v)
	{
		Stack<DirectedEdge> path = new Stack<DirectedEdge>();
		for(DirectedEdge e = edgeTo[v]; e!= null; e = edgeTo[e.from()])
			path.push(e);
		return path;
	}
	
	private void relax(DirectedEdge e)
	{
		int v = e.from(),w = e.to();
		if(distTo[w] > distTo[v] + e.weight()){
			distTo[w] = distTo[v] + e.weight();
			edgeTo[w] = e;
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

package com.example.algs.graph;

import com.example.algs.In;
import com.example.algs.StdOut;
import com.example.algs.collections.ST;

public class SymbolDiGraph {
	private ST<String,Integer> st;		//String -> index
	private String [] keys;			//index -> String
	private DiGraph G;
	public SymbolDiGraph(String stream,String sp) {
		// TODO Auto-generated constructor stub
		st = new ST<String,Integer>();
		In in = new In(stream);
		while(in.hasNextLine()){
			String [] a = in.readLine().split(sp);
			for	(int i = 0; i < a.length; i++)
				if	(!st.contains(a[i]))
					st.put(a[i], st.size());
		}
		keys = new String[st.size()];
		for(String key:st.keys())
			keys[st.get(key)] = key;
		G = new DiGraph(st.size());
		in = new In(stream);
		while(in.hasNextLine()){
			String [] a = in.readLine().split(sp);
			int v = st.get(a[0]);
			for	(int i = 1; i < a.length; i++)
				G.addEdge(v, st.get(a[i]));
		}
			
	}
	public boolean addEdge(String w,String v)
	{
		if(st.contains(w) && st.contains(v)){
			G.addEdge(st.get(w), st.get(v));
			return true;
		}
		return false;
	}
	
	public boolean contains(String s)	{ return st.get(s) != null;}
	public int index(String s)			{ return st.get(s);}
	public String name(int index)		{ return keys[index];}
	public DiGraph G()					{ return G;}
	public String toString()
	{
		String str = "";
		for(int v = 0; v < G.V(); v++){
			str += "("+v+")"+name(v)+":";
			for(int w:G.adj(v)){
				str+= "("+w+")"+name(w);
			}
			str+="\n";
		}
		return str;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SymbolDiGraph SG = new SymbolDiGraph("digraph3.txt","/");
		StdOut.println(SG);
		//DirectedDFS dfs = new DirectedDFS(SG.G(),0);
		DirectedCycle cycle = new DirectedCycle(SG.G());
		StdOut.println(cycle.hasCycle());
		if(SG.addEdge("Neural Networks", "Calculus")){
			cycle = new DirectedCycle(SG.G());
			StdOut.println(cycle.hasCycle());
			String str = "";
			int count = 0;
			for(int i:cycle.cycle()){
				if(count++ == 0)
					str += "("+i+")"+SG.name(i);
				else
					str += "=>"+"("+i+")"+SG.name(i);
			}
			StdOut.println("cycle:"+str);
		}
	}

}

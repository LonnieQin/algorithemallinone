package com.example.algs.graph;

import com.example.algs.In;
import com.example.algs.StdIn;
import com.example.algs.StdOut;
import com.example.algs.collections.ST;

public class SymbolGraph {

	private ST<String,Integer> st;	//String -> Index
	private String [] keys;			//index -> String
	private Graph G;				//the graph
	
	public SymbolGraph(String stream,String sp) {
		// TODO Auto-generated constructor stub
		st = new ST<String,Integer>();
		In in = new In(stream);
		while(in.hasNextLine())
		{
			String [] a = in.readLine().split(sp);
			for	(int i = 0; i < a.length; i++)
				if	(!st.contains(a[i]))
					st.put(a[i], st.size());
		}
		keys = new String[st.size()];
		for	(String name : st.keys())
			keys[st.get(name)] = name;
		
		G = new Graph(st.size());
		in = new In(stream);
		while(in.hasNextLine())
		{
			String [] a = in.readLine().split(sp);
			int v = st.get(a[0]);
			for	(int i = 1; i < a.length; i++)
				G.addEdge(v, st.get(a[i]));
		}
			
	}
	
	public boolean contains(String key)
	{
		return st.contains(key);
	}
	
	public int index(String key)
	{
		return st.get(key);
	}

	public String name(int v)
	{
		return keys[v];
	}
	
	Graph G()
	{
		return G;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SymbolGraph sg = new SymbolGraph("movies-hero.txt","/");
		Graph G = sg.G();
	
		for(int i = 0; i < sg.keys.length; i++)
			StdOut.println(i+" "+sg.name(i));
		StdOut.println("==============================");
		for(String key:sg.st.keys())
			StdOut.println(sg.st.get(key)+" "+key);
		while(StdIn.hasNextLine())
		{
			String source = StdIn.readLine();
			for (int w:G.adj(sg.index(source)))
				StdOut.println("  "+sg.name(w));
		}
		StdOut.println(G.maxDegree()+" "+G.avgDegree());
	}

}

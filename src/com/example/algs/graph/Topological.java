package com.example.algs.graph;

import com.example.algs.In;
import com.example.algs.StdOut;

public class Topological {

	private Iterable<Integer> order;	// topological
	public Topological(DiGraph G) {
		// TODO Auto-generated constructor stub
		DirectedCycle cyclefinder = new DirectedCycle(G);
		if(!cyclefinder.hasCycle()){
			DepthFirstOrder dfs = new DepthFirstOrder(G);
			order = dfs.reversePost();
		}
	}
	public Topological(EdgeWeightedDigraph G){
		EdgeWeightedDirectedCycle finder = new EdgeWeightedDirectedCycle(G);
		if(!finder.hasCycle()){
			DepthFirstOrder dfs = new DepthFirstOrder(G);
			order = dfs.reversePost();
		}
	}
	
	public Iterable<Integer> order()
	{
		return order;
	}
	
	public boolean isDAG()
	{	return order != null;}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SymbolDiGraph SG = new SymbolDiGraph("digraph3.txt","/");
		Topological top = new Topological(SG.G());
		for (int v: top.order())
			StdOut.println(SG.name(v));
		EdgeWeightedDigraph G = new EdgeWeightedDigraph(new In("tinyEWDAG.txt"));
		top = new Topological(G);
		for	(int v:top.order()){
			StdOut.println(v);
		}
	}

}

package com.example.algs.graph;

import com.example.algs.In;
import com.example.algs.StdOut;

public class TwoColor {
	private boolean [] marked;
	private boolean [] color;
	private boolean isTwoColorable = true;
	public TwoColor(Graph G) {
		// TODO Auto-generated constructor stub
		marked = new boolean[G.V()];
		color = new boolean[G.V()];
		for	(int s = 0; s < G.V(); s++)
			if(!marked[s])
				dfs(G,s);
	}

	private void dfs(Graph G, int v) {
		// TODO Auto-generated method stub
		StdOut.println("dfs(G,"+v+")");
		marked[v] = true;
		for(int w:G.adj(v))
			if(!marked[w])
			{
				color[w] = !color[v];
				dfs(G,w);
			}
			else if (color[w] == color[v]) isTwoColorable = false;
	}
	
	public boolean isBipartite()
	{	return isTwoColorable;}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Graph g = Graph.fromString(new In("graph2color.txt"));
		StdOut.println(g);
		TwoColor twoColor = new TwoColor(g);
		StdOut.println("is two colorable:"+twoColor.isTwoColorable);
	}

}

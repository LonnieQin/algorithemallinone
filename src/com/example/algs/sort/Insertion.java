package com.example.algs.sort;

import com.example.algs.Array;

public class Insertion extends Sorter {

	@Override
	public void sort(Comparable[] a) {
		// TODO Auto-generated method stub
		for(int i = 0; i < a.length; i++)
			for(int j = i+1; j < a.length; j++)
				if(less(a[j],a[i])) exch(a,i,j);
	}
	
	public void sort(String [] a, int lo, int hi,int d)
	{
		for(int i = lo; i <= hi; i++)
			for(int j = i; j > lo && a[j].substring(d).compareTo(a[j-1].substring(d))<0;j--)
				exch(a,j,j-1);
	}

	@Override
	public void visualSort(Integer[] a) {
		// TODO Auto-generated method stub
		printArray(a);
		for(int i = 0; i < a.length; i++)
			for(int j = i+1; j < a.length; j++)
				if(less(a[j],a[i])) vExch(a,i,j);
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Integer [] a = Array.generateRandomInteger(100, 0, 1000);
		new Insertion().visualSort(a);
		Array.print(Array.IntegersToInts(a));
	}


}

package com.example.algs.sort;

import java.awt.Color;

import com.example.algs.Array;
import com.example.algs.StdDraw;
import com.example.algs.StdOut;
import com.example.algs.StdString;

public class Merge extends Sorter {
	boolean arise = true;
	public boolean animated = false;
	Comparable [] aux;
	Integer [] intAux;
	public Merge() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void sort(Comparable[] a) {
		// TODO Auto-generated method stub
		aux = new Comparable[a.length];
		this.sort(a,0,a.length-1);
	}
	public void sort(Comparable [] a, int lo, int hi)
	{
		StdOut.println("sort(a,"+lo+","+hi+")");
		if(hi <= lo) return;
		int mid = lo+(hi-lo)/2;
		sort(a,lo,mid);
		sort(a,mid+1,hi);
		merge(a,lo,mid,hi);
	}
	@Override
	public void visualSort(Integer[] a) {
		// TODO Auto-generated method stub
		this.visualSort(a, 0,a.length-1);
	}
	public void visualSort(Integer [] a, int lo, int hi)
	{
		if(hi <= lo) return;
		int mid = lo+(hi-lo)/2;
		visualSort(a,lo,mid);
		visualSort(a,mid+1,hi);
		this.visualMerge(a,lo,mid,hi);
	}
	
	public void merge(Comparable [] a, int lo, int mid, int hi)
	{
		StdOut.println("merge(a,"+lo+","+mid+","+hi+")");
		int i = lo, j = mid+1;
		for(int k = lo; k <= hi; k++)
			aux[k] = a[k];
		for(int k = lo; k <= hi; k++)
		{
			if		(i > mid) 				a[k] = aux[j++];
			else if (j > hi)				a[k] = aux[i++];
			else if	(less(aux[j],aux[i]))	a[k] = aux[j++];
			else							a[k] = aux[i++];
			
		}
	}
	
	int max;
	public void visualMerge(Integer [] a, int lo, int mid, int hi)
	{
		if(intAux == null){
			intAux = new Integer[a.length];
			int N = a.length;
			int h = 1;
			StdDraw.setPenColor(Color.BLUE);
			for(int k = 0; k <a.length;k++){
				double x = 1.0*k/a.length;
				double y = a[k]/(2.0*max);
				double rw = 0.5/a.length;
				double rh = a[k]/(2.0*max);
				StdDraw.filledRectangle(x, y, rw, rh);
			}
			for(int i = 0; i < N; i++)
			{
				if(a[i] > max)
					max = a[i];
			}
			StdDraw.setPenColor(Color.BLUE);
			for(int k = 0; k <a.length;k++){
				double x = 1.0*k/a.length;
				double y = a[k]/(2.0*max);
				double rw = 0.5/a.length;
				double rh = a[k]/(2.0*max);
				StdDraw.filledRectangle(x, y, rw, rh);
			}
		}
		int i = lo, j = mid+1;
		for(int k = lo; k <= hi; k++)
		{
			intAux[k] = a[k];
		}
		for(int k = lo; k <= hi; k++)
		{
			if		(i > mid) 				a[k] = intAux[j++];
			else if (j > hi)				a[k] = intAux[i++];
			else if	(less(intAux[j],intAux[i]))	a[k] = intAux[j++];
			else							a[k] = intAux[i++];
			StdDraw.setPenColor(Color.WHITE);
			StdDraw.filledRectangle(1.0*k/a.length, 0.5, 0.5/a.length, 0.5);
			StdDraw.setPenColor(Color.BLUE);
			double x1 = 1.0*k/a.length;
			double y1 = a[k]/(2.0*max);
			double rw1 = 0.5/a.length;
			double rh1 = a[k]/(2.0*max);
			StdDraw.filledRectangle(x1, y1, rw1, rh1);
		}
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Integer [] ints = Array.generateRandomInteger(10, 0, 100);
		new Merge().sort(ints);
		Integer [] integers = Array.generateRandomInteger(1000, 0, 100);
		new Merge().visualSort(integers);
	}



}

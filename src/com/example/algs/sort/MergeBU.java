package com.example.algs.sort;

import java.awt.Color;

import com.example.algs.Array;
import com.example.algs.StdDraw;
import com.example.algs.StdOut;
import com.example.algs.StopWatch;

public class MergeBU extends Sorter {
	Comparable [] aux;
	private Integer[] integerAux;
	@Override
	public void sort(Comparable[] a) {
		// TODO Auto-generated method stub
		int N = a.length;
		aux = new Comparable[N];
		for(int sz = 1; sz < N; sz *= 2)
			for(int lo = 0; lo < N-sz; lo += sz+sz)
				merge(a,lo,lo+sz-1,Math.min(lo+2*sz-1, N-1));
	}
	public void merge(Comparable [] a,int lo, int mid, int hi)
	{
		//StdOut.println("merger(a,"+lo+","+mid+","+hi+")");
		int i = lo;
		int j = mid+1;
		for(int k = lo; k <= hi; k++)
			aux[k] = a[k];
		for(int t = lo; t <= hi; t++)
			if		(i > mid)	a[t] = aux[j++];
			else if	(j > hi)	a[t] = aux[i++];
			else if	(less(aux[i],aux[j])) a[t] = aux[i++];
			else a[t] = aux[j++];
			
	}
	int max;
	@Override
	public void visualSort(Integer[] a) {
		// TODO Auto-generated method stub
		int N = a.length;
		max = a[0];
		for(int i = 0; i < a.length; i++)
			if(a[i]>max)max = a[i];
		integerAux = new Integer[N];
		StdDraw.setPenColor(Color.BLUE);
		for(int k = 0; k <a.length;k++){
			double x = 1.0*k/a.length;
			double y = a[k]/(2.0*max);
			double rw = 0.5/a.length;
			double rh = a[k]/(2.0*max);
			StdDraw.filledRectangle(x, y, rw, rh);
		}
		
		for(int sz = 1; sz < N; sz = sz+sz)
		{
			for(int lo = 0; lo < N-sz; lo += sz+sz)
			{
				StdDraw.setPenColor(Color.WHITE);
				int len = Math.min(lo+sz+sz-1, N-1)-lo+1;
				//StdDraw.filledRectangle(((lo+len)/2)/(double)N, 0.5, 0.5*(len)/N, 0.5);
				if(N < 800)
				for(int i = lo; i < len; i++)
				{
					
					StdDraw.setPenColor(Color.WHITE);
					StdDraw.filledRectangle(i/(double)N, 0.5, 0.5/N, 0.5);
					StdDraw.setPenColor(Color.BLACK);
					StdDraw.filledRectangle(i/(double)N, 0.5*a[i]/max, 0.5/N, 0.5*a[i]/max);
				}
				
				//StdDraw.show(1);
				this.visualMerge(a,lo,lo+sz-1,Math.min(lo+2*sz-1, N-1));
			}
			StdDraw.show();
		}
	}	
	private void visualMerge(Integer[] a, int lo, int mid, int hi) {
		// TODO Auto-generated method stub
		int i = lo;
		int j = mid+1;
		for(int k = lo; k <= hi; k++)
			integerAux[k] = a[k];
		for(int t = lo; t <= hi; t++)
		{
			if		(j > hi)	a[t] = integerAux[i++];
			else if	(i > mid)	a[t] = integerAux[j++];
			else if	(less(integerAux[i],integerAux[j])) a[t] = integerAux[i++];
			else a[t] = integerAux[j++];
			
			StdDraw.setPenColor(Color.WHITE);
			StdDraw.filledRectangle(1.0*t/a.length, 0.5, 0.5/a.length, 0.5);
			StdDraw.setPenColor(Color.BLUE);
			double x1 = 1.0*t/a.length;
			double y1 = a[t]/(2.0*max);
			double rw1 = 0.5/a.length;
			double rh1 = a[t]/(2.0*max);
			StdDraw.filledRectangle(x1, y1, rw1, rh1);
		}
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int N = 100;
		double prev = test(N);
		for(N = 200; N < 100000; N*=2)
		{
			StdDraw.clear();
			double present = test(N);
			double rate = present / prev;
			StdOut.println(N+" "+ present+" "+rate);
			prev = present;
		}
	}
	public static double test(int N)
	{
		Integer[] integers = Array.generateRandomInteger(N, 0, 10000);
		StopWatch timer = new StopWatch();
		new MergeBU().visualSort(integers);
		//Array.print(Array.IntegersToInts(integers));
		return timer.elapsedTime();
	}

}

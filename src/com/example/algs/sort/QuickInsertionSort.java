package com.example.algs.sort;

import com.example.algs.Array;
import com.example.algs.StdOut;
import com.example.algs.StdRandom;
import com.example.algs.StopWatch;

public class QuickInsertionSort extends Sorter {
	static int CUTOFF = 3;
	public void sort(Comparable[] a) {
		// TODO Auto-generated method stub
		StdRandom.shuffle(a);
		sort(a,0,a.length-1);
	}

	private void sort(Comparable[] a, int lo, int hi) {
		// TODO Auto-generated method stub
		if(hi <= lo+CUTOFF-1) 
		{
			for(int i = lo; i <= hi; i++)
				for(int j = i+1; j <= hi; j++)
					if(less(a[j],a[i])) exch(a,i,j);
		}
		int j = partition(a,lo,hi);
		sort(a,lo,j-1);
		sort(a,j+1,hi);
	}

	private int partition(Comparable[] a, int lo, int hi) {
		// TODO Auto-generated method stub
		int i = lo,j = hi+1;
		while(true)
		{
			while(less(a[++i],a[lo]))
				if(i == hi)	break;
			while(less(a[lo],a[--j]))
				if(j == lo) break;
			if(i >= j) break;
			exch(a,i,j);
		}
		exch(a,lo,j);	
		return j;
	}

	@Override
	public void visualSort(Integer[] a) {
		// TODO Auto-generated method stub
		StdRandom.shuffle(a);
		printArray(a);
		visualSort(a,0,a.length-1);
	}
	
	private void visualSort(Integer[] a, int lo, int hi) {
		// TODO Auto-generated method stub
		if(hi <= lo+CUTOFF-1) 
		{
			for(int i = lo; i <= hi; i++)
				for(int j = i+1; j <= hi; j++)
					if(less(a[j],a[i])) vExch(a,i,j);
			return;
		}
		int j = visualPartition(a,lo,hi);
		visualSort(a,lo,j-1);
		visualSort(a,j+1,hi);
	}
	
	private int visualPartition(Integer [] a, int lo, int hi) {
		// TODO Auto-generated method stub
		int i = lo,j = hi+1;
		while(true)
		{
			while(less(a[++i],a[lo]))
				if(i == hi)	break;
			while(less(a[lo],a[--j]))
				if(j == lo) break;
			if(i >= j) break;
			vExch(a,i,j);
		}
		vExch(a,lo,j);	
		return j;
	}	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Integer [] a = Array.generateRandomInteger(1000, 0, 1000);
		StopWatch sw = new StopWatch();
		new QuickInsertionSort().visualSort(a);
		StdOut.println("Time:"+sw.elapsedTime());
		Array.print(Array.IntegersToInts(a));
	}
}

package com.example.algs.sort;

import java.awt.Color;

import com.example.algs.Array;
import com.example.algs.StdDraw;
import com.example.algs.StdOut;
import com.example.algs.StdString;

public class Selection extends Sorter {
	public void sort(Comparable [] a)
	{
		int N =  a.length;
		for(int i = 0; i < N; i++)
		{
			int selected = i;
			for(int j = i+1; j < N; j++)
				if(less(a[j],a[selected])) selected = j;
			exch(a,i,selected);
		}
	}
	public void visualSort(Integer [] a) {
		// TODO Auto-generated method stub
		int N =  a.length;
		Integer max =a[0];
		for(int i = 0; i < N; i++)
		{
			if(a[i] > max)
				max = a[i];
		}
		
		for(int i = 0; i < N; i++)
		{
			int selected = i;
			for(int j = i+1; j < N; j++)
				if(less(a[j],a[selected])) selected = j;
			exch(a,i,selected);
			StdDraw.clear();
			for(int k = 0; k <a.length;k++){
				double x = 1.0*k/a.length;
				double y = a[k]/(2.0*max);
				double rw = 0.5/a.length;
				double rh = a[k]/(2.0*max);
				if(k == i || k == selected)
					StdDraw.setPenColor(Color.RED);
				StdDraw.filledRectangle(x, y, rw, rh);
				if(k == i || k == selected)
					StdDraw.setPenColor(Color.BLACK);
			}
		}
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String [] a = StdString.generateRandomString(1000, 3, 10);
		new Selection().sort(a);
		StdOut.println("Is sorted:"+isSorted(a));
		Integer [] integers = Array.generateRandomInteger(100, new Integer(0), new Integer(100));
		new Selection().visualSort(integers);
	}
}

package com.example.algs.sort;

import java.awt.Color;
import com.example.algs.StdDraw;
import com.example.algs.StdOut;
import com.example.algs.StdRandom;
import com.example.algs.StdString;

public class Shell extends Sorter {
	@Override
	public void sort(Comparable[] a) {
		// TODO Auto-generated method stub
		int N = a.length;
		int h = 1;
		while(h < N/3)
		{
			h = 3*h+1;
		}
		while(h >= 1)
		{
			for(int i = h; i < N; i++)
			{
				for(int j = i; j>=h &&  less(a[j],a[j-h]); j -= h)
				{
					exch(a,j,j-h);
				}
			}
			h = h / 3;
		}
	}
	
	public void visualSort(Integer [] a) {
		// TODO Auto-generated method stub
		int N = a.length;
		int h = 1;
		Integer max =a[0];
		for(int i = 0; i < N; i++)
		{
			if(a[i] > max)
				max = a[i];
		}
		while(h < N/3)
		{
			h = 3*h+1;
		}
		StdDraw.setPenColor(Color.BLUE);
		for(int k = 0; k <a.length;k++){
			double x = 1.0*k/a.length;
			double y = a[k]/(2.0*max);
			double rw = 0.5/a.length;
			double rh = a[k]/(2.0*max);
			StdDraw.filledRectangle(x, y, rw, rh);
		}
		while(h >= 1)
		{
			for(int i = h; i < N; i++)
			{
				//记录要交换的两个数的x、y坐标和宽高坐标
				double x1=0,y1=0,rw1=0,rh1=0;
				double x2=0,y2=0,rw2=0,rh2=0;
				for(int j = i; j>=h &&  less(a[j],a[j-h]); j -= h)
				{
					exch(a,j,j-h);
					//vExch(a,j,j-h);
					StdDraw.setPenColor(Color.WHITE);
					//把交换的两个数空白
					StdDraw.filledRectangle(1.0*j/a.length, 0.5, 0.5/a.length, 0.5);
					StdDraw.filledRectangle(1.0*(j-h)/a.length, 0.5, 0.5/a.length, 0.5);
					StdDraw.setPenColor(Color.BLUE);
					if(j != i){
						//如果不是第一次的循环，清空原来交换的
						StdDraw.setPenColor(Color.BLUE);
						StdDraw.filledRectangle(x1, y1, rw1, rh1);
						StdDraw.filledRectangle(x2, y2, rw2, rh2);
					}
					x1 = 1.0*j/a.length;
					y1 = a[j]/(2.0*max);
					rw1 = 0.5/a.length;
					rh1 = a[j]/(2.0*max);
					x2 = 1.0*(j-h)/a.length;
					y2 = a[j-h]/(2.0*max);
					rw2 = 0.5/a.length;
					rh2 = a[j-h]/(2.0*max);
					StdDraw.setPenColor(Color.RED);
					StdDraw.filledRectangle(x1, y1, rw1, rh1);
					StdDraw.filledRectangle(x2, y2, rw2, rh2);
					StdDraw.setPenColor(Color.BLUE);
					StdDraw.show(100);
					StdDraw.setPenColor(Color.BLUE);
					StdDraw.filledRectangle(x1, y1, rw1, rh1);
					StdDraw.filledRectangle(x2, y2, rw2, rh2);
				}
			}
			h = h / 3;
		}
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String [] a = StdString.generateRandomString(1000, 3, 10);
		new Selection().sort(a);
		StdOut.println("Is sorted:"+isSorted(a));
		Integer [] integers = new Integer[50];
		for(int i = 0; i < integers.length; i++)
		{
			integers[i] = StdRandom.uniform(1, 500);
		}
		new Shell().visualSort(integers);
	}

}

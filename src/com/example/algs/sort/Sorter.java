package com.example.algs.sort;

import java.awt.Color;

import com.example.algs.StdDraw;
import com.example.algs.StdOut;
import com.example.algs.StdString;

public abstract class Sorter {
	public abstract void sort(Comparable [] a);
	public abstract void visualSort(Integer [] a);
	protected static boolean less(Comparable v, Comparable w)
	{
		return v.compareTo(w) < 0;
	}
	protected static boolean larger(Comparable v, Comparable w)
	{
		return v.compareTo(w) > 0;
	}
	protected static void exch(Comparable [] a, int i, int j)
	{	Comparable t = a[i]; a[i] = a[j]; a[j] = t;}
	public static boolean isSorted(Comparable [] a)
	{
		for(int i = 1; i < a.length; i++)
			if(less(a[i],a[i-1])) return false;
		return true;
	}
	private static void reverse(Comparable [] a)
	{
		int N = a.length;
		for(int i = 0; i < a.length/2; i++)
		{
			exch(a,i,N-i-1);
		}
	}

	protected int max;
	protected void printArray(Integer [] a)
	{
		max = a[0];
		for(int i = 0; i < a.length; i++)
		{
			if(a[i] > max) max = a[i];
		}
		StdDraw.setPenColor(Color.GREEN);
		for(int i = 0; i <a.length;i++){
			double x = 1.0*i/a.length;
			double y = a[i]/(2.0*max);
			double rw = 0.5/a.length;
			double rh = a[i]/(2.0*max);
			StdDraw.filledRectangle(x, y, rw, rh);
		}
	}
	
	public void vExch(Integer [] a, int i, int j)
	{
		exch(a,i,j);
		double x1=0,y1=0,rw1=0,rh1=0;
		double x2=0,y2=0,rw2=0,rh2=0;
		StdDraw.setPenColor(Color.WHITE);
		//把交换的两个数空白
		StdDraw.filledRectangle(1.0*i/a.length, 0.5, 0.5/a.length, 0.5);
		StdDraw.filledRectangle(1.0*j/a.length, 0.5, 0.5/a.length, 0.5);
		StdDraw.setPenColor(Color.BLUE);
		x1 = 1.0*i/a.length;
		y1 = a[i]/(2.0*max);
		rw1 = 0.5/a.length;
		rh1 = a[i]/(2.0*max);
		x2 = 1.0*j/a.length;
		y2 = a[j]/(2.0*max);
		rw2 = 0.5/a.length;
		rh2 = a[j]/(2.0*max);
		StdDraw.setPenColor(Color.RED);
		StdDraw.filledRectangle(x1, y1, rw1, rh1);
		StdDraw.filledRectangle(x2, y2, rw2, rh2);
		StdDraw.setPenColor(Color.BLUE);
		StdDraw.show(50);
		StdDraw.setPenColor(Color.BLUE);
		StdDraw.filledRectangle(x1, y1, rw1, rh1);
		StdDraw.filledRectangle(x2, y2, rw2, rh2);
	}
}

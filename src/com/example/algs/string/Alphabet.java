package com.example.algs.string;

import java.util.Arrays;

import com.example.algs.StdOut;
import com.example.algs.collections.SeparateChainingHashST;

public class Alphabet {

	SeparateChainingHashST<Character,Integer> st;
	char keys[];
	int R;
	public Alphabet(String s) {
		// TODO Auto-generated constructor stub
		R = 0;
		char [] chars = new char[s.length()];
		for(int i = 0; i < s.length(); i++)
			chars[i] = s.charAt(i);
		Arrays.sort(chars);		
		st = new SeparateChainingHashST<Character,Integer>();
		for(int i = 0; i < chars.length;i++)
		{
			if(st.get(chars[i]) == null)
				st.put(chars[i], R++);
		}
		StdOut.println(R+" "+st.size());
		keys = new char[st.size()];
		for(char c:st.keys())
			keys[st.get(c)] = c;
		
	}
	
	public char toChar(int index){
		if(index < 0 || index >= keys.length)
			return Character.MAX_VALUE;
		return keys[index];
	}
	public int toIndex(char c){
		return st.get(c);
	}
	public boolean contains(char c){
		return st.get(c) != null;
	}
	public int R(){
		return R;
	}
	public int lgR(){
		return (int) Math.ceil(Math.log(R));
	}
	
	public int [] toIndices(String s){
		int [] indices = new int[s.length()];
		for(int i = 0; i < s.length(); i++)
			indices[i] = toIndex(s.charAt(i));
		return indices;
	}
	
	public String toChars(int indices[]){
		StringBuilder sb = new StringBuilder();
		for(int i : indices)
			sb.append(toChar(i));
		return sb.toString();
	}
	
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < R; i++)
		{
			sb.append(i+" "+keys[i]+"\n");
		}
		return sb.toString();
	}

	/**
	 * General Used Alphabet
	 */
	public static final Alphabet BINARY = new Alphabet("01");
	public static final Alphabet DNA = new Alphabet("ACTG");
	public static final Alphabet OCTAL = new Alphabet("01234567");
	public static final Alphabet DECIMAL = new Alphabet("0123456789");
	public static final Alphabet HEXADECIMAL = new Alphabet("0123456789ABCDEF");
	public static final Alphabet PROTEIN = new Alphabet("ACDEFGHIKLMNPQRSTVWY");
	public static final Alphabet LOWERCASE = LOWERCASE();
	public static final Alphabet UPPERCASE = UPPERCASE();
	public static final Alphabet BASE64 = BASE64();
	public static final Alphabet UNICODE16 = UNICODE16();
	private static Alphabet LOWERCASE() {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		for(char c = 'a'; c <= 'z';c++)
			sb.append(c);
		Alphabet alpha = new Alphabet(sb.toString());
		return alpha;
	}
	private static Alphabet UNICODE16() {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		
		return new Alphabet(sb.toString());
	}

	public static final Alphabet UPPERCASE(){
		StringBuilder sb = new StringBuilder();
		for(char c = 'A'; c <= 'Z';c++)
			sb.append(c);
		Alphabet alpha = new Alphabet(sb.toString());
		return alpha;
	}
	public static final Alphabet BASE64(){
		StringBuilder sb = new StringBuilder();
		for(char c = 'A'; c <= 'Z'; c++)
			sb.append(c);
		for(char c = 'a'; c <= 'z'; c++)
			sb.append(c);
		for(char c = '0'; c <= '9'; c++)
			sb.append(c);
		sb.append('+');
		sb.append('/');
		return new Alphabet(sb.toString());
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StdOut.println("Binary:");
		StdOut.println(Alphabet.BINARY);
		StdOut.println("DNA:");
		StdOut.println(Alphabet.DNA);
		StdOut.println("Base64:");
		StdOut.println(Alphabet.BASE64);
		StdOut.println("binary:"+Alphabet.BINARY.lgR()+" DNA:"+Alphabet.DNA.lgR()+" OCTAL:"+Alphabet.OCTAL.lgR());
		StdOut.println("\u1000");
		StdOut.println("\uFFFFF");
	
	}



}

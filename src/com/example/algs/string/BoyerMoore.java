package com.example.algs.string;

import com.example.algs.StdOut;

public class BoyerMoore {

	private int [] right;
	private String pat;
	public BoyerMoore(String pat) {
		// TODO Auto-generated constructor stub
		this.pat = pat;
		int M = pat.length();
		int R = 256;
		right = new int[R];
		for(int c = 0; c < R; c++)
			right[c] = -1;
		for(int j = 0; j < M; j++)
			right[pat.charAt(j)] = j;
	}

	public int search(String txt)
	{
		int N = txt.length();
		int M = pat.length();
		int skip;
		for(int i = 0; i < N-M; i += skip)
		{
			skip = 0;
			for(int j = M-1; j >=0;j--)
				if(pat.charAt(j) != txt.charAt(i+j))
				{
					skip = j - right[txt.charAt(i+j)];
					if(skip < 1) skip = 1;
					break;
				}
			if(skip == 0)return i;
		}
		return N;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub	
		String pat = "password";
		String txt = "egi4jgirejuoeurt8rupasswordeigjegjWFGIEGJ";
		BoyerMoore bm = new BoyerMoore(pat);
		StdOut.println("Text:   "+txt);
		int offset = bm.search(txt);
		StdOut.print("Pattern:");	
		for(int i = 0; i < offset; i++)
			StdOut.print(" ");
		StdOut.println(pat);
		StdOut.println(offset);
	}

}

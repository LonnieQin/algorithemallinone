package com.example.algs.string;

import com.example.algs.StdOut;
import com.example.algs.StdString;
import com.example.algs.StopWatch;
import com.example.algs.sort.Insertion;

public class MSD {
	static String [] aux;
	static int R = 256;
	private final static int M = 15;
	public static void sort(String [] a){
		aux = new String[a.length];
		sort(a,aux,0,a.length-1,0);
	}
	public static void sort(String [] a,String [] aux,int lo, int hi,int d)
	{
		if (hi <= lo+M){ 
			new Insertion().sort(a,lo,hi,d);
			/*
			for(int i = lo; i <= hi; i++)
				for(int j = i+1; j <= hi; j++)
					if(charAt(a[j],d) < charAt(a[i],d)){
						String t = a[j];
						a[j] = a[i];
						a[i] = t;
					}
			*/
			return;
		}
		int [] count = new int[R+2];
		for(int i = lo; i <= hi; i++)
			count[charAt(a[i],d)+2]++;
		for(int r = 0; r < R+1; r++)
			count[r+1] += count[r];
		for(int i = lo; i <= hi; i++)
			aux[count[charAt(a[i],d)+1]++] = a[i];
		for(int i = lo; i <= hi; i++)
			a[i] = aux[i-lo];
		for(int r = 0; r < R; r++)
			sort(a,aux,lo+count[r],lo+count[r+1]-1,d+1);

	}
	public static int charAt(String s, int d)
	{
		if(d < s.length()) return s.charAt(d);
		else return -1;
	}
	/**
	/**
	 * @param args
	 */
	public static void  main(String[] args) {
		// TODO Auto-generated method stub
		for(int N = 10000; N < 10001; N*=2){
			StopWatch timer = new StopWatch();
			String [] strs = StdString.generateRandomString(N, 3, 10);
			MSD.sort(strs);
			for(int i = 0; i < strs.length; i++)
				StdOut.println(strs[i]);
		StdOut.println("Elapsed Time:"+timer.elapsedTime());
		}
	}

}

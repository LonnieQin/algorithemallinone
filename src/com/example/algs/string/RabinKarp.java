package com.example.algs.string;

import com.example.algs.StdOut;

public class RabinKarp {
	private String pat;		//pattern(only nedded for Las Vegas)
	private long patHash;	//pattern hash value
	private int M;			//pattern length
	private long Q;			//a large prime
	private int R = 256;	//alphabet size
	private long RM;		//R^(M-1) % Q
	public RabinKarp(String pat) {
		// TODO Auto-generated constructor stub
		this.pat = pat;					//save pattern (only needed for Las Vegas)
		this.M = pat.length();	
		Q = longRandomPrime();
		RM = 1;
		for	(int i = 1; i <= M-1; i++)	
			RM = (R*RM)%Q;				//in removing leading digit.
		patHash = hash(pat,M);
	}
	
	public boolean check(int i)
	{	return true;}

	private long hash(String key, int M) {
		// TODO Auto-generated method stu
		long h = 0;
		for(int j = 0; j < M; j++)
			h = (R*h + key.charAt(j))%Q;
		return h;
	}

	private long longRandomPrime() {
		// TODO Auto-generated method stub
		return 997;
	}

	private int search(String txt)
	{
		int N = txt.length();
		long txtHash = hash(txt,M);
		if(patHash == txtHash && check(0)) return 0;	//Match at beginning.
		for(int i = M; i < N; i++)
		{
			txtHash = (txtHash + Q - RM*txt.charAt(i-M) % Q) % Q;
			txtHash = (txtHash*R + txt.charAt(i)) % Q;
			if(patHash == txtHash)
				if(check(i-M+1))return i - M + 1;		//match
		}
		return N;										//no match found
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String pat = "password";
		String txt = "egi4jgirejuoeurt8rupasswordeigjegjWFGIEGJ";
		RabinKarp rb = new RabinKarp(pat);
		StdOut.println("Text:   "+txt);
		int offset = rb.search(txt);
		StdOut.print("Pattern:");	
		for(int i = 0; i < offset; i++)
			StdOut.print(" ");
		StdOut.println(pat);
		StdOut.println(offset);
	}

}

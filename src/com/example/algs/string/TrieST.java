package com.example.algs.string;

import com.example.algs.StdOut;
import com.example.algs.StdRandom;
import com.example.algs.StdString;
import com.example.algs.StopWatch;
import com.example.algs.collections.Queue;

public class TrieST<Value> {
	private static class Node {
		private Object value;
		private Node[] next = new Node[R];
	}
	private Node root = new Node();
	public static int R = 256;
	private int size = 0;

	/**
	 * put key-value pair into the table(remove key if value is null)
	 * @param key
	 * @param val
	 */
	public void put(String key,Value val)
	{
		root = put(root,key,val,0);
	}
	
	private Node put(Node x,String key,Value val,int d)
	{	
		if(x == null) 
			x = new Node();
		if(d == key.length()){
			if(x.value == null)size++;
			x.value = val;
			return x;
		}
		char c = key.charAt(d);
		x.next[c] = put(x.next[c],key,val,d+1);
		return x;
	}
	
	/**
	 * value pair with key
	 * @param key
	 * @return
	 */
	public Value get(String key)
	{
		Node x = get(root,key,0);
		if(x == null) return null;
		return (Value)x.value;
	}
	
	private Node get(Node x, String key, int d) {
		// TODO Auto-generated method stub
		if(x == null) return null;
		if(d == key.length())return x;
		char c = key.charAt(d);
		return get(x.next[c],key,d+1);
	}

	/**
	 * remove key(and its value)
	 * @param key
	 */
	public void delete(String key)
	{
		/*
		Node x = get(root,key,0);
		if(x == null)return;
		else{
			if(x.value != null)
			{
				size--;
				x.value = null;
			}
		}
		*/
		root = delete(root,key,0);
	}
	
	private Node delete(Node x, String key,int d)
	{
		if(x == null) return null;
		if(d == key.length()){
			x.value = null;
			size--;
		}
		else{
			char c = key.charAt(d);
			x.next[c] = delete(x.next[c],key,d+1);
		}
		
		if(x.value != null) return x;
		for(char c = 0; c < R; c++)
			if(x.next[c] != null) return x;
		return null;	
	}
	
	/**
	 * is there a value paired with key?
	 * @param key
	 * @return
	 */
	public boolean contains(String key)
	{
		return get(key) != null;
	}
	
	/**
	 * is the table absent?
	 * @return
	 */
	public boolean isEmpty()
	{
		return size==0;
	}
	
	/**
	 * the longest key that is prefix of s
	 * @param s
	 * @return
	 */
	public String longestPrefixOf(String s)
	{
		int length = search(root,s,0,0);
		return s.substring(0, length);
	}
	
	private int search(Node x,String s,int d,int length)
	{
		if(x == null) return length;
		if(x.value != null) length = d;
		if(d == s.length()) return length;
		char c = s.charAt(d);
		return search(x.next[c],s,d+1,length);
	}
	
	/**
	 * all the keys having s as a prefix
	 * @param s
	 * @return
	 */
	public Iterable<String> keysWithPrefix(String pre)
	{
		Queue<String> q = new Queue<String>();
		collect(get(root,pre,0),pre,q);
		return q;
	}
	private void collect(Node x,String pre,Queue<String> q)
	{
		if(x == null) return;
		if(x.value != null) q.enqueue(pre);
		for(char c = 0; c < R; c++)
			collect(x.next[c],pre+c,q);
	}
	/**
	 * number of key value pairs
	 * @return
	 */
	public int size()
	{
		return size;
	}
	
	Iterable<String> keys()
	{
		return keysWithPrefix("");
	}
	
	public Iterable<String> keysThatMatch(String pat)
	{
		Queue<String> q = new Queue<String>();
		collect(root,"",pat,q);
		return q;
	}
	
	public void collect(Node x,String pre,String pat,Queue<String> q)
	{
		int d = pre.length();
		if(x == null) return;
		if(d == pat.length() && x.value != null) q.enqueue(pre);
		if(d == pat.length()) return;
		
		char next = pat.charAt(d);
		for(char c = 0; c < R; c++)
			if(next == '.' ||next == c)
				collect(x.next[c],pre+c,pat,q);
	}
		
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		String [] keys = StdString.generateRandomString(100, 3, 15);
		TrieST<Integer> st = new TrieST<Integer>();
		for(String key:keys)
			st.put(key, StdRandom.uniform(1, 10000));
		StopWatch timer = new StopWatch();
		for(String key:keys)
		{
			int i = st.get(key);
			//StdOut.println(key+" "+st.get(key));
		}
        StdOut.println("Elapsed Millis:"+timer.elapsedMillis()); 
        for(String key:st.keysWithPrefix(""))
        	StdOut.println(key+" "+st.get(key));
        st.put("ADS",33);
        StdOut.println(st.get("ADS")+" "+st.size());
        st.delete("ADS");
        StdOut.println(st.get("ADS")+" "+st.size());
	}

}

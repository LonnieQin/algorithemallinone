package com.example.compiler;

public class Num extends Token {
	private int value;
	public Num(int v) {
		super(Tag.NUM);
		value = v;
	}

}

package com.example.compiler;

import java.io.IOException;

public class Parser {

	static int lookahead;
	
	public Parser() throws IOException {
		// TODO Auto-generated constructor stub
		lookahead = System.in.read();

	}
	
	void expr() throws IOException{
		term();
		while(true){
			if(lookahead == '+') {
				match('+');term();System.out.write('+');
			}
			else if( lookahead == '-'){
				match('-');term();System.out.println('-');
			}
			else if (lookahead == '*'){
				match('*');term();System.out.println('*');
			}
			else if (lookahead == '/'){
				match('/');term();System.out.println('/');
			}
			else return;
		}
	}

	private void term() throws IOException {
		// TODO Auto-generated method stub
		if(Character.isDigit((char)lookahead)){
			System.out.write((char)lookahead);match((char)lookahead);
		}
		else throw new Error("syntax error");
	}

	private void match(char t) throws IOException {
		// TODO Auto-generated method stub
		if( lookahead == t) lookahead = System.in.read();
		else throw new Error("syntax error");
	}

}

package com.example.compiler;

import com.example.algs.Array;
import com.example.algs.StdOut;

public class QuickSort {
	public static void quicksort(int a[],int m, int n)
	{
		int i,j;
		int v,x;
		if(n <= m) return;
		i = m-1; j = n; v = a[n];
		while(true){
			do ++i; while(a[i] < v);
			do --j; while(a[j] > v);
			if(i >= j)break;
			x = a[i]; a[i] = a[j]; a[j] = x;
		}
		x = a[i]; a[i] = a[n]; a[n] = x;
		quicksort(a,m,j);quicksort(a,i+1,n);
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] a = Array.generateRandomNumber(1000, 1, 10000);
		quicksort(a,0,999);
		//StdOut.println("A");
	}

}

package com.example.pattern.factory;

import com.example.pattern.factory.pizza.CheesePizza;
import com.example.pattern.factory.pizza.ChicagoStyleCheesePizza;
import com.example.pattern.factory.pizza.ChicagoStyleClamPizza;
import com.example.pattern.factory.pizza.ChicagoStylePepperoniPizza;
import com.example.pattern.factory.pizza.ChicagoStyleVeggiePizza;
import com.example.pattern.factory.pizza.ClamPizza;
import com.example.pattern.factory.pizza.NYStyleCheesePizza;
import com.example.pattern.factory.pizza.PepperoniPizza;
import com.example.pattern.factory.pizza.Pizza;
import com.example.pattern.factory.pizza.VeggiePizza;

public class ChicagoPizzaStore extends PizzaStore{
	@Override
	Pizza createPizza(String type) {
		// TODO Auto-generated method stub
		Pizza pizza = null;
		if(type.equals("cheese")){
			pizza = new ChicagoStyleCheesePizza();
		}else if(type.equals("pepperoni")){
			pizza = new ChicagoStylePepperoniPizza();
		}else if(type.equals("clam")){
			pizza = new ChicagoStyleClamPizza();
		}else if(type.equals("veggie")){
			pizza = new ChicagoStyleVeggiePizza();
		}
		return pizza;
	}

}

package com.example.pattern.factory;

import com.example.algs.StdOut;
import com.example.algs.StdRandom;
import com.example.pattern.factory.pizza.Pizza;

public class Client {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String [] pizzas = new String[]{"cheese","pepperoni","clam","veggie"};
		SimplePizzaFactory factory = new SimplePizzaFactory();
		ChicagoPizzaStore store = new ChicagoPizzaStore();
		for(int i = 0; i < 100; i++){
			Pizza p = store.orderPizza(pizzas[StdRandom.uniform(0, pizzas.length)]);
			StdOut.println(p.toString());
		}
	}

}

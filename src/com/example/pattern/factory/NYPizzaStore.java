package com.example.pattern.factory;

import com.example.pattern.factory.pizza.NYStyleCheesePizza;
import com.example.pattern.factory.pizza.NYStyleClamPizza;
import com.example.pattern.factory.pizza.NYStylePepperoniPizza;
import com.example.pattern.factory.pizza.NYStyleVeggiePizza;
import com.example.pattern.factory.pizza.Pizza;

public class NYPizzaStore extends PizzaStore {
	@Override
	Pizza createPizza(String type) {
		Pizza pizza = null;
		// TODO Auto-generated method stub
		if(type.equals("cheese")){
			pizza = new NYStyleCheesePizza();
		}else if(type.equals("pepperoni")){
			pizza = new NYStylePepperoniPizza();
		}else if(type.equals("clam")){
			pizza = new NYStyleClamPizza();
		}else if(type.equals("veggie")){
			pizza = new NYStyleVeggiePizza();
		}
		return pizza;
	}

}

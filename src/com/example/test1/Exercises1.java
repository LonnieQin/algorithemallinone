package com.example.test1;

import java.awt.Color;
import java.util.Arrays;

import com.example.I.Mathematic;
import com.example.I.VisualCounter;
import com.example.algs.Array;
import com.example.algs.Interval1D;
import com.example.algs.Interval2D;
import com.example.algs.Out;
import com.example.algs.Point2D;
import com.example.algs.StdDraw;
import com.example.algs.StdIn;
import com.example.algs.StdOut;
import com.example.algs.StdRandom;
import com.example.algs.StdString;

public class Exercises1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//StdOut.printf("a is %f,b s %lf,c is %d", a,b,33);
		//t1_1_1();
		//t1_1_2();
		//t1_1_4();
		//t1_1_5();
		//t1_1_6();
		//t1_1_7();
		//t1_1_8();
		//t1_1_9();	
		//t1_1_11();
		//t1_1_12();
		//t1_1_14();
		//t1_1_15();
		//t1_1_16();
		//t1_1_18();
		//t1_1_19();
		//t1_1_20();
		//t1_1_21();
		//t1_1_22();
		//t1_1_23();
		//t1_1_27();
		//t1_2_1();
		//t1_2_2();
		//t1_2_3();
		//t1_2_4();
		//t1_2_5();
		//t1_2_6();
		//t1_2_7();
		//t1_2_8();
		t1_2_10();
	}
	private static void t1_2_1()
	{
		int N = 1000;
		Point2D [] pd = new Point2D[N];
		for(int i = 0; i < N; i++)
		{
			pd[i] = new Point2D(StdRandom.random(),StdRandom.random());
			pd[i].draw();
		}
		int [] closestPo = new int[N];
		double [] mindist = new double[N];
		for(int i = 0; i < N; i++)
		{
			closestPo[i] = Integer.MAX_VALUE;
			mindist[i] = Double.MAX_VALUE;
		}
		for(int i = 0; i < N; i++)
		{
			for(int j = i+1; j < N; j++)
			{
				double dist = pd[i].distanceTo(pd[j]);
				if(dist < mindist[i])
				{
					mindist[i] = dist;
					mindist[j] = dist;
					closestPo[i] = j;
					closestPo[j] = i;
				}
			}
		}
		Out o = new Out("1.2.1.txt");
		for(int i = 0; i < 1000; i++)
		{
			StdDraw.line(pd[i].x(), pd[i].y(), pd[closestPo[i]].x(), pd[closestPo[i]].y());
			o.println("The closest position from "+i+" to "+ closestPo[i]+" is "+mindist[i]);
		}
		o.close();
	}
	private static void t1_2_2()
	{
		Interval1D [] intervals = new Interval1D[1000];
		for(int i = 0; i < 1000; i++)
		{
			while(true)
			{
				double left = StdRandom.random() * 100;
				double right = StdRandom.random() * 200;
				if(left < right)
				{
					intervals[i] = new Interval1D(left,right);
					break;
				}
			}
		}
		Out o = new Out("1.2.2.txt");
		
		int count = 0;
		for(int i = 0; i < 1000; i++)
		{
			for(int j = i+1; j < 1000; j++)
			{
				if(intervals[i].intersects(intervals[j]))
					o.print("("+i+","+j+")"+"	");
				if(++count % 10 == 0) o.println();
			}
		}
		
		o.close();
			
	}
	private static void t1_2_3(){
		int N = 1000;
		int min = 100;
		int max = 1000;
		Interval2D [] intervals = new Interval2D[N];
		for(int i = 0; i < intervals.length; i++)
		{
			Interval1D x,y;
				double xl,xr;
				double yt,yd;
				xr = StdRandom.uniform(min+10,max);
				xl = StdRandom.uniform(min, xr);
				yd = StdRandom.uniform(min+10,max);
				yt = StdRandom.uniform(min, yd);
				x = new Interval1D(xl,xr);
				y = new Interval1D(yt,yd);
				intervals[i] = new Interval2D(x,y);
		}
		StdDraw.setPenColor(Color.RED);
		StdDraw.setScale(min, max);
		for(int i = 0; i < intervals.length; i++)
		{
			StdDraw.setPenColor(new Color(255,0,0,255-(i*255)/intervals.length));
			intervals[i].draw();
		}
	}
	private static void t1_2_4(){
		String str1 = "hello";
		String str2 = str1;
		str1 = "world";
		StdOut.println(str1);
		StdOut.println(str2);
	}
	private static void t1_2_5(){
		String s = "Hello World";
		s = s.toUpperCase();
		s = s.substring(6, 11);
		StdOut.println(s);
	}
	private static void t1_2_6(){
		String a = "cdefghi";
		String b = "fghicde";
		StdOut.println(StdString.isCircularShift(a, b));
	}
	private static void t1_2_7(){
		String s = "DEAR TOM , I LOVE YOU VERY MUCH.I WISH YOU WELL";
		StdOut.println(StdString.reverse(s));
	}
	private static void t1_2_8(){
		
	}
	private static void t1_2_9(){}
	private static void t1_2_10(){
		StdDraw.setCanvasSize();
		StdDraw.setPenColor(StdDraw.BOOK_BLUE);
		int N = 10000;
		int max = 1000;
		VisualCounter va = new VisualCounter("va",N,max);
		for(int i = 0; i < N; i++)
		{
			if(StdRandom.random() > 0.3)
			{
				va.increment();
			}
			else
			{
				va.decrement();
			}
		}
	}
	private static void t1_2_11(){}
	private static void t1_2_12(){}
	private static void t1_2_13(){}
	private static void t1_2_14(){}
	private static void t1_2_15(){}
	private static void t1_2_16(){}
	private static void t1_2_17(){}
	private static void t1_2_18(){}
	private static void t1_2_19(){}
	
	private static void t1_1_27(){
		StdOut.println("Before");
		double b = Mathematic.binominal(100, 50, 0.5);
		StdOut.println("After");
		System.out.println("b:"+b);
	}
	private static void t1_1_23() {
		// TODO Auto-generated method stub
		int p = 4,q=128;
		StdOut.println("Gcd of "+p+" and "+q+" is "+Mathematic.gcd(p, q));
	}
	private static void t1_1_22() {
		// TODO Auto-generated method stub
		int [] a = Array.generateRandomNumber(1000, 1, 100);
		Arrays.sort(a);
		int i = a[490];
		int po = Array.binarySearch(i, a);
		System.out.println("Location of "+i+" is "+po);
		if(po > 0)
		System.out.println("Check:"+a[po]);
	}
	private static void t1_1_21()
	{
		String [] strs = new String[100];
		for(int i = 0; i < 100; i++)
		{
			strs[i] = "String"+(i+1);
		}
		int [] a = Array.generateRandomNumber(100, 1, 100);
		int [] b = Array.generateRandomNumber(100, 1, 100);
		double [] c = new double[100];
		for(int i = 0; i < 100; i++)
		{
			c[i] = a[i] / (double)b[i];
		}
		for(int i = 0; i < 100; i++)
		{
			StdOut.printf("%-10s",strs[i]);
		}
		StdOut.println();
		for(int i = 0; i < 100; i++)
		{
			StdOut.printf("%-10d", a[i]);
		}
		StdOut.println();
		for(int i = 0; i < 100; i++)
		{
			StdOut.printf("%-10d", b[i]);
		}
		StdOut.println();
		for(int i = 0; i < 100; i++)
		{
			StdOut.printf("%-10.3f", c[i]);
		}
		StdOut.println();
	}

	private static void t1_1_20() {
		// TODO Auto-generated method stub
		long a = Mathematic.factorial(20);
		for(int i = 0; i < 20; i++)
		{
			System.out.println("factorial("+i+")="+Mathematic.factorial(i));
			System.out.println("factorial("+i+")="+Math.pow(Math.E, Math.log(Mathematic.factorial(i))));
		}
		for(int i = 0; i <100;i++)
		{
			System.out.println("LNFactorial("+i+")="+Mathematic.LNFactorial(i));
			System.out.println("LNFactorial("+i+")="+Math.log(Mathematic.factorial(i)));
		}
	}

	private static void t1_1_19() {
		// TODO Auto-generated method stub
		for(int i = 0; i < 100; i++)
		{
			System.out.println("Fibonacci["+i+"]="+Mathematic.fibonacci(i));
		}
		
		Mathematic mc = new Mathematic();
		for(int i = 0; i < 100; i++)
		{
			System.out.println("Fibonacci["+i+"]="+mc.Fibonacci(i));
		}
	}

	private static void t1_1_18() {
		// TODO Auto-generated method stub
		StdOut.println(mystery(2,25));
		StdOut.println(mystery(3,11));
	}
	public static int mystery(int a, int b)
	{
		if(b == 0) return 0;
		if(b % 2 == 0) return mystery(a+a,b/2);
		return mystery(a+a,b/2)+a;
	}

	private static void t1_1_16() {
		// TODO Auto-generated method stub
		StdOut.println(exR1(25));
	}
	public static String exR1(int n )
	{
		if(n <= 0) return "";
		return exR1(n-3)+n+exR1(n-2)+n;
	}

	private static void t1_1_15() {
		// TODO Auto-generated method stub
		int array1 [] = Array.generateRandomNumber(5000, 0, 499);
		int array[] = new int[500];
		for(int i = 0 ; i < array1.length; i++)
		{
			array[array1[i]]++;
		}
		Array.drawArray(array, new Color[]{Color.red,Color.GREEN,Color.blue,Color.gray,Color.LIGHT_GRAY});
	}	

	private static void t1_1_14() {
		// TODO Auto-generated method stub
		int N = 333;
		int i = 0;
		while(Math.pow(2, ++i) < N);
		StdOut.println(Math.pow(2, i-1));
	}

	private static void t1_1_12() {
		// TODO Auto-generated method stub
		int [] a = new int[10];
		for(int i = 0; i < 10; i++)
		{
			a[i] = 9-i;
		}
		for(int i = 0; i < 10; i++)
		{
			a[i] = a[a[i]];
		}
		for(int i = 0; i < 10; i++)
		{
			System.out.println(a[i]);
		}
	}

	private static void t1_1_11() {
		// TODO Auto-generated method stub
		boolean [][] booleanArray = Array.generateBoolean2DArray(10, 12);
		//System.out.println(booleanArray[0][0]);
		Array.printBoolean2DArray(booleanArray, '*', ' ');
	}

	private static void t1_1_9() {
		// TODO Auto-generated method stub
		char [] sb = new char[32];
		sb[0] = '0';
		for(int i = 1; i < 32; i++)
		{
			sb[i] = (StdRandom.uniform(1, 1000) % 2 == 0)? '0':'1';
		}
		for(int i = 0; i < sb.length; i++){
			StdOut.print(sb[i]);
		}
		StdOut.println();
		int num = 0;
		for(int i = 1; i <sb.length; i++)
		{
			if(sb[i] == '1')
			{
				num += Math.pow(2, sb.length-i-1);
			}
		}
		StdOut.println(num);
		String str = ""+num;
	}

	private static void t1_1_8() {
		// TODO Auto-generated method stub
		System.out.println('b');
		System.out.println('b'+'c');
		System.out.println((char)('a'+4));
	}

	private static void t1_1_7() {
		// TODO Auto-generated method stub
		double t = 9.0;
		while(Math.abs(t-9.0/t) > .001)
			t = (9.0/t + t) / 2.0;
		StdOut.printf("%.5f\n", t);
		
		int sum = 0;//1+2+3+...+999+1000
		for(int i = 1; i < 1000; i++)
			for(int j = 0; j < i; j++)
				sum++;
		StdOut.println(sum);
		
		sum = 0;
		for(int i = 1; i < 1000; i *= 2)
			for( int j = 0; j < 1000; j++)
				sum ++;
		StdOut.println(sum);
	}

	private static void t1_1_6() {
		// TODO Auto-generated method stub
		long f = 0;
		long g = 1;
		for(int i = 0; i <= 1000; i++)
		{
			StdOut.println(f);
			f = f+g;
			g = f - g;
		}
	}

	private static void t1_1_5() {
		// TODO Auto-generated method stub
		double x,y;
		StdOut.print("Please input x and y:");
		x = StdIn.readDouble();
		y = StdIn.readDouble();
		if((x>=0 && x <= 1 )&& (y >= 0 && y <= 1))
		{
			StdOut.print(true);
		}
		else
		{
			StdOut.print(false);
		}
	}

	private static void t1_1_4() {
		// TODO Auto-generated method stub
		int a = 1, b = 2, c = 333;
		if(a > b)
		{
			c = 0;
		}
		else
		{
			b = 0;
		}
		StdOut.println(c);
	}

	private static void t1_1_2() {
		// TODO Auto-generated method stub
		double a = (1+2.236)/2;
		double b = 1+2+3+4.0;
		boolean c = (4.1 >= 4);
		String d = 1+2+"3";
		System.out.println("a:"+a+" b:"+b+" c:"+c+" d:"+d);
		
	}

	private static void t1_1_1() {
		// TODO Auto-generated method stub
		double a = (0+15)/2;
		double b = 2.0e-6 * 100000000.1;
		boolean c = true && false || true && false;
		StdOut.println(c);
	}
	
}

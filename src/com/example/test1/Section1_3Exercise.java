package com.example.test1;

import com.example.algs.StdOut;
import com.example.algs.collections.Queue;
import com.example.algs.collections.Stack;

public class Section1_3Exercise {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Exec6();
	}
	public static void Exec1(){}//done
	public static void Exec2(){
		Stack<String> stack = new Stack<String>();
		String str = "it was - the best - of times --- it was - the - -";
		String [] strs = str.split(" ");
		for(String s : strs)
			stack.push(s);
		while(!stack.isEmpty())
		{
			StdOut.print(stack.pop()+" ");
		}
	}
	public static void Exec3(){
		//a
		Stack<Integer> stack1 = new Stack<Integer>();
		stack1.push(0);
		stack1.push(1);
		stack1.push(2);
		stack1.push(3);
		stack1.push(4);
		pp(stack1);
		pp(stack1);
		pp(stack1);
		pp(stack1);
		pp(stack1);
		stack1.push(5);
		stack1.push(6);
		stack1.push(7);
		stack1.push(8);
		stack1.push(9);
		while(!stack1.isEmpty())
		{
			pp(stack1);
		}
		Stack<Integer> stack2 = new Stack<Integer>();
	}
	public static void pp(Stack s)
	{
		StdOut.print(s.pop()+" ");
	}
	public static void Exec4(){
		String str = "()[[]]{()}";
		Stack<Character> left = new Stack<Character>();
		for(int i = 0;i<str.length();i++)
		{
			char c = str.charAt(i);
			if(c == '[' || c == '(' || c == '{')
				left.push(c);
			if(c == ']' || c == '}')
			{
				if(!left.isEmpty())
				{
					if(c - left.pop() != 2)
					{
						StdOut.println("Error");
					}
				}
				else
				{
					StdOut.println("Error");
				}
			}
			if(c == ')')
			{
				if(!left.isEmpty())
				{
					if(c - left.pop() != 1)
					{
						StdOut.println("Error");
					}
				}
				else
				{
					StdOut.println("Error");
				}
			}
		}
		if(!left.isEmpty())
			StdOut.print("Error");
	}
	public static void Exec5(){
		Stack<Integer> stack = new Stack<Integer>();
		int N = 50;
		while(N > 0)
		{
			stack.push(N % 2);
			N = N / 2;
		}
		for(int i : stack)
		{
			StdOut.print(i);
		}
		StdOut.println();
	}
	public static void Exec6(){
		Stack<String> stack = new Stack<String>();
		Queue<String> q = new Queue<String>();
		for(int i = 0; i < 100; i++)
			q.enqueue("String"+i);
		while(!q.isEmpty())
			stack.push(q.dequeue());
		while(!stack.isEmpty())
			q.enqueue(stack.pop());
		while(!q.isEmpty())
			StdOut.println(q.dequeue());
		stack.push("stack1");
		for(int i = 0; i < 10; i++)
		{
			StdOut.println(stack.peek());
		}
	}
	public static void Exec7(){}
	public static void Exec8(){}
	public static void Exec9(){}
	public static void Exec10(){}
	public static void Exec11(){}
	public static void Exec12(){}
	public static void Exec13(){}
	public static void Exec14(){}
	public static void Exec15(){}
	public static void Exec16(){}
	public static void Exec17(){}
	public static void Exec18(){}
	public static void Exec19(){}
	public static void Exec20(){}
	public static void Exec21(){}
	public static void Exec22(){}
	public static void Exec23(){}
	public static void Exec24(){}
	public static void Exec25(){}
	public static void Exec26(){}
	public static void Exec27(){}
	public static void Exec28(){}
	public static void Exec29(){}
	public static void Exec30(){}
	public static void Exec31(){}
	public static void Exec32(){}
	public static void Exec33(){}
	public static void Exec34(){}
	public static void Exec35(){}
	public static void Exec36(){}
	public static void Exec37(){}
	public static void Exec38(){}
	public static void Exec39(){}
	public static void Exec40(){}
	public static void Exec41(){}
	public static void Exec42(){}
	public static void Exec43(){}
	public static void Exec44(){}
	public static void Exec45(){}
	public static void Exec46(){}
	public static void Exec47(){}
	public static void Exec48(){}
	public static void Exec49(){}
	public static void Exec50(){}
	
}
